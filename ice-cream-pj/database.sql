CREATE DATABASE  IF NOT EXISTS `ice-cream` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ice-cream`;
-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: ice-cream
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('admin','admin1');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_role`
--

DROP TABLE IF EXISTS `app_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `app_role` (
  `ROLE_ID` bigint NOT NULL,
  `ROLE_NAME` varchar(30) NOT NULL,
  PRIMARY KEY (`ROLE_ID`),
  UNIQUE KEY `APP_ROLE_UK` (`ROLE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_role`
--

LOCK TABLES `app_role` WRITE;
/*!40000 ALTER TABLE `app_role` DISABLE KEYS */;
INSERT INTO `app_role` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_USER');
/*!40000 ALTER TABLE `app_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_user`
--

DROP TABLE IF EXISTS `app_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `app_user` (
  `USER_ID` bigint NOT NULL,
  `USER_NAME` varchar(36) NOT NULL,
  `ENCRYTED_PASSWORD` varchar(128) NOT NULL,
  `ENABLED` bit(1) NOT NULL,
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `APP_USER_UK` (`USER_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_user`
--

LOCK TABLES `app_user` WRITE;
/*!40000 ALTER TABLE `app_user` DISABLE KEYS */;
INSERT INTO `app_user` VALUES (1,'dbadmin1','$2a$10$PrI5Gk9L.tSZiW9FXhTS8O8Mz9E97k2FZbFvGFFaSsiTUIl.TCrFu',_binary ''),(2,'dbuser1','$2a$10$PrI5Gk9L.tSZiW9FXhTS8O8Mz9E97k2FZbFvGFFaSsiTUIl.TCrFu',_binary '');
/*!40000 ALTER TABLE `app_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer` (
  `customer_id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` text NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `avatar` varchar(2048) DEFAULT NULL,
  `expired_date` date NOT NULL,
  `enable_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'fortune','8l9Dp3HQloE=','Fortune','123 Pasteur','0901234567','forton@icecream.com',1,'1987-01-01','images/uploaded/noavatar.gif','2018-12-25',1),(2,'frances','8l9Dp3HQloE=','Frances','456 Alexandre de Rhodes','0911234567','frances@icecream.com',1,'1991-01-01','images/uploaded/noavatar.gif','2018-12-12',1),(3,'dorothye','8l9Dp3HQloE=','Dorothye','789 Yersin','0921234567','dorothye@icecream.com',1,'1990-01-01','images/uploaded/noavatar.gif','2018-12-12',1),(4,'christobell','Cuwus/jwiqVUDOdgDW8Lgw==','Im Christobell','Seoul - Korea','091234567','christobell@gmail.com',0,'1990-10-05','images/uploaded/noavatar.gif','2018-02-11',1),(5,'lovely','Cuwus/jwiqVUDOdgDW8Lgw==','NoName','Seoul - Korea','0912345678','noname@gmail.com',0,'1990-10-10','images/uploaded/noname.gif','2018-02-11',1),(6,'ILoveVietNam','Cuwus/jwiqVUDOdgDW8Lgw==','Barberye','Ho Chi Minh','0912345679','ilovevn@gmail.com',1,'1990-10-11','images/uploaded/noname.gif','2018-02-11',1),(7,'ILoveHoChiMinh','Cuwus/jwiqVUDOdgDW8Lgw==','Annas','Ho Chi Minh','0932345659','ilovehochiminh@gmail.com',1,'1985-10-11','images/uploaded/noavatar.gif','2018-02-11',1),(8,'ILoveHue','Cuwus/jwiqVUDOdgDW8Lgw==','Allice','Ho Chi Minh','0912345659','ilovehue@gmail.com',1,'1991-10-11','images/uploaded/0.jpg','2018-02-11',1),(9,'ILoveIndia','Cuwus/jwiqVUDOdgDW8Lgw==','n0 name','Ho Chi Minh','0912345659','iloveindia@gmail.com',1,'1991-10-11','images/uploaded/1.jpg','2018-02-11',1),(18,'hung.nguyen','$2a$10$eC5QxtDORla2quJ7TCz0WuP2ywWK8pjOvVRclNr/AISKnqjGL.GOm','Nguyen Ngoc Hung1','Q91','9744697501','hung.nguyen@gmail.com.vn',1,'1996-10-29','images/nobita.png','2023-08-13',1),(28,'\'\'\'\'\'\'\'\'\'\'','$2a$10$2fA0vsI/AYP9zXnW8W5fhuhacGezGb2Z0p4hZ1LMOlWyNm1qEqTsC','<p>','','',NULL,1,NULL,'images/demo-user.mp4','2020-04-20',1);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `faq` (
  `faq_id` int NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq`
--

LOCK TABLES `faq` WRITE;
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
INSERT INTO `faq` VALUES (1,'Where can I get coupons for Parlor products?','Occasionally we offer coupons and samples on our website and Facebook page.'),(2,'Do your products contain allergens or gluten?','Some products do contain allergens, including gluten. However, most of our ice creams and frozen dairy desserts are naturally gluten free. We are currently updating our packaging to make it easy for you to identify all of our naturally gluten free variants. It is our policy that when any of the eight most common allergens (milk, eggs, fish, wheat, tree nuts, peanuts, soybeans and crustaceans) occur in any of our products they will be listed inside the ingredient statement in plain language.'),(3,'How Can I Buy Book Online?','You Should Login to The Member Page and Leave your Personal infomation there .Our deliver service base on this.'),(4,'Is my daughter safe on Parlor Icecream? ','User safety is our most important concern. Always Icecream is using a wide range of methods and processes to ensure user safety and privacy. The site is content is continuously scanned for inappropriate content using both software and manual reviews. Users can also report any concerns or problems they encounter to Always Icecream staff. Furthermore, self-policing mechanisms are in place to encourage proper behavior and etiquette. Lastly, parents receive regular email reports about the activities and learning progress of their daughter. '),(5,'If my recipe is Prized what way i receive my prize money?','You can go direct to Parlor Shop or your banking...'),(6,'If my recipe is Prized what way i receive my prize money?','You can go direct to Parlor Shop or your banking...'),(8,'how to redireact to other page when login success !','I don\'t know');
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feedback` (
  `feedback_id` int NOT NULL AUTO_INCREMENT,
  `full_name` varchar(50) DEFAULT 'Guest',
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` VALUES (1,'Ellen','what a wonderful recipe','the recipes are so great,I can make Ice-Creame by myself'),(2,'Florence','Thanks alot','Thanks very much .After became members i have been seen the recipe'),(3,'Grace','Dear','Love the Ice Creame recipe.All Best Ice cream in town every time'),(4,'Helen','Best Vani cream','The Ice Cream is great and they have a wide variety of flavours, amazing milkshakes and desserts.'),(5,'Ellen','how to post your recipe','You can view introduce in here http://ỉntroduce'),(7,'Nguyen Ngoc Hung','nhap test','test');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online_order`
--

DROP TABLE IF EXISTS `online_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `online_order` (
  `order_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact` varchar(2048) NOT NULL,
  `address` varchar(2048) NOT NULL,
  `book_cost` float NOT NULL,
  `paying_option` varchar(100) NOT NULL,
  `order_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online_order`
--

LOCK TABLES `online_order` WRITE;
/*!40000 ALTER TABLE `online_order` DISABLE KEYS */;
INSERT INTO `online_order` VALUES (1,'Robert J.Becam','RobertBecam@yahoo.com.vn','2501','231 Washinton',49.99,'VISA','0022-03-22 00:00:00',0),(2,'C.R Ronado','C.R.Ronado@yahoo.com.vn','2501','231 Washinton',49.99,'VISA','2020-02-02 00:00:00',0),(3,'L.Messi','L.Messi@yahoo.com.vn','2501','231 Washinton',49.99,'VISA','2020-12-04 00:00:00',0),(4,'Ryan Giggs','Ryan.Giggs@yahoo.com.vn','2501','231 Washinton',49.99,'VISA','2020-12-04 00:00:00',1),(5,'Frank Lampard','frankld@gmail.com','2501','231 Washinton',49.99,'VISA','2020-12-04 00:00:00',1),(6,'David James','DavidJames@gmail.com','2501','231 Washinton',49.99,'VISA','2020-12-04 00:00:00',1),(7,'Gareth Barry','GarethBarry@gmail.com','2501','231 Washinton',49.99,'VISA','2020-12-04 00:00:00',1),(8,'Gary Speed','GarySpeed@gmail.com','2501','231 Washinton',49.99,'VISA','2020-12-04 00:00:00',1),(9,'Emile Heskey','EmileHeskey@gmail.com','2501','231 Washinton',49.99,'VISA','2020-12-04 00:00:00',1),(10,'Rio Ferdinand','Rio Ferdinand@gmail.com','2501','231 Washinton',49.99,'VISA','2020-12-04 00:00:00',1),(11,'Jamie Carragher','JamieCarragher@gmail.com','2501','231 Washinton',49.99,'VISA','2020-12-04 00:00:00',1),(12,'Philip Neville','PhilipNeville@gmail.com','2501','231 Washinton',49.99,'VISA','2020-12-04 00:00:00',1),(13,'Steven Gerrard','StevenGerrard@gmail.com','2501','231 Washinton',49.99,'VISA','2020-12-04 00:00:00',1),(14,'Brad Friedel','BradFriedel@gmail.com','2501','231 Washinton',49.99,'VISA','2020-12-04 00:00:00',1),(16,'hung','ces.ricky@yahoo.com.vn','hung','111/12/14 Quang trung Q9',49.99,'SMS Banking','1996-10-29 00:00:00',1),(17,'hung','ces.ricky@yahoo.com.vn','hung','111/12/14 E QT Q9',49.99,'SMS Banking','1996-10-29 00:00:00',1),(18,'hung','ces.ricky@yahoo.com.vn','hung','111/12/14E',49.99,'SMS Banking','1996-10-29 00:00:00',1),(19,'hung','ces.ricky@yahoo.com.vn','hung','111/12/14E QT Q9',49.99,'SMS Banking','1996-10-29 00:00:00',1),(20,'1','a@a','1','1',49.99,'VISA','0001-01-01 00:00:00',1);
/*!40000 ALTER TABLE `online_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persistent_logins`
--

DROP TABLE IF EXISTS `persistent_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persistent_logins` (
  `username` varchar(64) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persistent_logins`
--

LOCK TABLES `persistent_logins` WRITE;
/*!40000 ALTER TABLE `persistent_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `persistent_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prize_history`
--

DROP TABLE IF EXISTS `prize_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prize_history` (
  `user_recipe_id` int NOT NULL,
  `enable_status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_recipe_id`),
  CONSTRAINT `prize_history_ibfk_1` FOREIGN KEY (`user_recipe_id`) REFERENCES `user_recipe` (`user_recipe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prize_history`
--

LOCK TABLES `prize_history` WRITE;
/*!40000 ALTER TABLE `prize_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `prize_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipe`
--

DROP TABLE IF EXISTS `recipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recipe` (
  `recipe_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `desciption` text NOT NULL,
  `details` text NOT NULL,
  `author` varchar(50) NOT NULL,
  `view_number` int NOT NULL DEFAULT '1',
  `upload_date` date NOT NULL,
  `enable_status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`recipe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe`
--

LOCK TABLES `recipe` WRITE;
/*!40000 ALTER TABLE `recipe` DISABLE KEYS */;
INSERT INTO `recipe` VALUES (1,'Banana chocolate chip ice cream','images/Banana chocolate chip ice cream(300x300).jpg','\n\r2/3 cup sugar.\n\r2 large eggs.\n\r1 1/2 tablespoons all-purpose flour.\n\r1/8 teaspoon salt.\n\r1 1/4 cups milk.\n\r2 large very ripe bananas.\n\r1 cup light cream.\n\r1 teaspoon vanilla extract.\n\r2 tablespoons banana liqueur','\n\rStep 1: In a medium mixing bowl, beat the sugar into the eggs until thickened and pale yellow. Beat in the flour and salt. Set aside.\n\rStep 2: Bring the milk to a boil in a heavy medium saucepan. Slowly beat the hot milk into the eggs and sugar. Pour the entire mixture back into the pan and place over low heat. Stir constantly with a whisk or wooden spoon until the custard thickens slightly. Be careful not to let the mixture boil or the eggs will scramble. Remove from the heat and pour the custard through a strainer into a large, clean bowl.\n\rStep 3: Mash the bananas with a potato masher until they are creamy, or puree them in a food processor. Add the banana puree to the hot custard and mix well. Allow the banana custard to cool slightly. Stir in the cream, vanilla.\n\rStep 4: Cover and refrigerate until cold or overnight. (My kids have never let me chill anything overnight, so I speed up the cooling process with an ice bath. Just make sure your mixture is completely chilled before adding to the ice cream machine).\n\rStep 5: Stir the chilled custard, and then freeze in 1 or 2 batches in your ice cream machine according to the manufacturer’s instructions.\n\rStep 6: When the ice cream is semi-frozen, add ½ cup of semi-sweet chocolate chips to the machine and let the machine mix the chocolate.\n\rStep 7: When finished, the ice cream will be soft but ready to eat. For firmer ice cream, transfer to a freezer-safe container and freeze at least 2 hours.','Bruce Weinstein',10,'2017-05-29',1),(2,'Cherry ice cream','images/Cherry ice cream(300x300).jpg','\n\r6 (approximately 2 1/4 pounds) ripe bananas.\n\r1 tablespoon fresh squeezed lemon juice.\n\r3/4 cup light corn syrup.\n\r1 vanilla bean, scraped.\n\r1 1/2 cups heavy cream','\n\r1.Place bananas in freezer and freeze overnight.\n\r2.Remove bananas from freezer and allow to thaw for 45 minutes to 1 hour.\n\r3.Peel bananas and place in bowl of food processor along with the lemon juice.\n\r4.Process for 10 to 15 seconds.\n\r5.Add corn syrup and vanilla bean seeds and turn processor on.\n\r6.Slowly pour in the heavy cream. Process until smooth.\n\r7.Chill mixture in refrigerator until it reaches 40 degrees.\n\r8.Transfer mixture to an ice cream maker and process according to manufacturers instructions.\n\r9.Place mixture in an airtight container and freeze for 3 to 6 hours before serving.','Bruce Weinstein',2,'2017-05-29',1),(3,'Blackcurrant ice cream','images/Blackcurrant ice cream(300x300).jpg','\n\r1 lb (450 g) fresh or frozen blackcurrants, defrosted if frozen.\n\r6 oz (175 g) sugar.\n\r10 fl oz (275 ml) double cream','\n\r1.Theres no need to take the stalks off the blackcurrants, just pile them, about one third of a pound (5 oz/150 g) at a time, into the sieve set over a mixing bowl, and mash like mad with a wooden spoon until you have extracted all the pulp and only the stalks, pips and skins are left in the sieve.\n\r2.Now place the sugar and 5 fl oz (150 ml) water in a saucepan over a medium heat, stir until all the sugar crystals have dissolved, then let it come to the boil, and boil for 3 minutes exactly. Then remove from the heat and stir the syrup into the fruit pulp. Whip the cream until it just begins to thicken. Be careful not to overwhip: it mustnt be thick, just floppy. Fold the cream into the fruit mixture until thoroughly blended. Pour it into the polythene box, and freeze in a freezer or in the ice-making compartment of a fridge turned to its coldest setting.\n\r3.As soon as the mixture begins to set (after about 3 hours) turn it out into a bowl and beat thoroughly. Then return it to the freezer (in the box) until set – about another 3 hours. Remove to the main part of the fridge about an hour before serving.','Bruce Weinstein',25,'2017-05-29',1),(4,'Butterscotch ice cream','images/Butterscotch ice cream(300x300).jpg','\n\r1 cup firmly packed brown sugar.\n\r2 tablespoons butter.\n\r1 tablespoon vanilla.\n\r1 1/2 cups whipping cream.\n\r2 cups half-and-half (light cream).\n\r6 large egg yolks','\n\r1. In a 1- to 2-quart pan over medium heat, stir brown sugar, butter, and vanilla until butter is melted, sugar is dissolved, and mixture is bubbly, 3 to 4 minutes. Whisk in 1/2 cup whipping cream until smooth; remove butterscotch mixture from heat.\n\r2. In a 3- to 4-quart pan over medium-high heat, combine remaining 1 cup whipping cream and the half-and-half; bring to a simmer.\n\r3. Meanwhile, in a bowl, beat egg yolks to blend. Whisk 1/2 cup of the warm cream mixture into egg yolks, then pour egg yolk mixture into pan with cream. Stir constantly over low heat just until mixture is slightly thickened, 2 to 4 minutes. Immediately remove from heat.\n\r4. Pour through a fine strainer into a clean bowl and whisk in butterscotch mixture. Chill until cold, stirring occasionally, about 2 hours; or cover and chill up to 1 day.\n\r5. Freeze mixture in an ice cream maker according to manufacturers instructions. Serve softly frozen, or transfer ice cream to an airtight container and freeze until firm, at least 6 hours or up to 1 week.Nutritional analysis is per 1/2 cup.','Bruce Weinstein',40,'2017-05-29',1),(5,'Cashew Caramel crunch ice cream','images/Cashew Caramel crunch ice cream(300x300).jpg','\n\r1/2 cup blanched almonds, toasted.\n\r1/2 cup caster sugar.\n\r2 litres vanilla ice-cream, softened.\n\r380g can Top n Fill Caramel.\n\r200g dark chocolate melts.\n\r60g copha','\n\rStep 1: Line an oven tray with baking paper. Sprinkle almonds over tray. Combine sugar and 1/2 cup warm water in a saucepan over low heat. Simmer, stirring, for 3 minutes. Increase heat to high. Bring to the boil. Boil, without stirring, for 10 minutes or until mixture is golden. Remove from heat. Stand for 1 minute to allow bubbles to subside. Pour over almonds. Allow to cool until set.\n\rStep 2: Peel baking paper from almond mixture. Cut almond mixture into small pieces. Combine ice-cream and almond mixture in a bowl. Fold caramel through ice-cream. Return ice-cream to container. Place in freezer overnight or until firm.\n\rStep 3: Scoop small balls of ice-cream onto a tray lined with baking paper. Freeze for 30 minutes.\n\rStep 4: Meanwhile, place chocolate and copha in a heatproof, microwave-safe bowl. Microwave on high (100%) for 1 to 2 minutes, stirring every minute with a metal spoon, or until smooth. Set aside. Allow to cool slightly.\n\rStep 5: Using a fork, dip the ice-cream balls in chocolate mixture. Return to prepared tray. Place in freezer for 30 minutes or until chocolate mixture is set. Serve.','Bruce Weinstein',50,'2017-05-29',1),(6,'Banana ice cream','images/Banana ice cream(300x300).jpg','\n\r2 cups (500 ml) heavy cream.\n\r1 cup (250 ml) whole milk.\n\r3/4 cup (150 g) granulated sugar.\n\rPinch of salt.\n\r1 vanilla bean.\n\r1 tsp. vanilla extract.\n\rAbout 2 ½ cups fresh cherries (will be 2 cups when pitted and quartered)','\n\r1.In a medium saucepan, heat 1 cup of the cream, sugar, and salt.\n\r2.Split the vanilla bean in half lengthwise with knife and scrape the seeds with spoon.\n\r3.Put both seeds and pod in the pan.\n\r4.Stir the mixture over medium heat until sugar completely dissolves.\n\r5.Remove from the heat and add the vanilla extract, the rest of the heavy cream (1 cup) and milk. Mix well.\n\r6.Chill the mixture in the refrigerator for several hours (or overnight) until completely cold.\n\r7.Pit cherries. By the way, a cherry pitter is a great investment ($13) if you have small children who loves cherries or you enjoy making cherry desserts every year - its super quick to pit cherries!\n\r8.Cut cherries into quarters.\n\r9.Take out the vanilla bean pod before churning. Process the mixture in your ice cream maker according to manufacturers instructions (about 25 minutes). Or follow.\n\r10.In last 5 minutes add in the cherries. My ice cream maker bowl is 1.5 QT so I had to stop churning and add cherries. Combine ice cream and cherries well.\n\r11.Put the ice cream in an airtight container and let it firm by freezing it for several hours. ','Bruce Weinstein',60,'2017-05-29',1),(7,'Chocolate almond ice cream','images/Chocolate almond ice cream(300x300).jpg','\n\r2 envelopes unflavored gelatin.\n\r6 tablespoons cold water.\n\r3 cups milk.\n\r3 cups sugar.\n\r1/4 teaspoon salt.\n\r3 eggs, lightly beaten.\n\r6 to 7 ounces unsweetened chocolate, melted.\n\r4 cups heavy whipping cream.\n\r2 teaspoons vanilla extract.\n\r1 cup sliced or slivered Diamond of California® Almonds, toasted','\n\r1.In a small bowl, sprinkle gelatin over cold water; let stand for at least 2 minutes. In a large heavy saucepan, heat the milk, sugar and salt until bubbles form around sides of pan. Whisk a small amount of hot mixture into the eggs. Return all to the pan, whisking constantly.\n\r2.Cook and stir over low heat until mixture is thickened and coats the back of a spoon. Remove from the heat. Stir in gelatin mixture until dissolved; stir in chocolate until blended. Cool quickly by placing pan in a bowl of ice water; stir for 2 minutes. Stir in cream and vanilla. Press plastic wrap onto surface of custard. Refrigerate for several hours or overnight.\n\r3.Fill cylinder of ice cream freezer two-thirds full; freeze according to manufacturers directions. Refrigerate remaining mixture until ready to freeze. When ice cream is frozen, stir in almonds. Transfer to a freezer container; freeze for 2-4 hours before serving.','Bruce Weinstein',70,'2017-05-29',1),(8,'Chocolate chip ice cream','images/Chocolate chip ice cream(300x300).jpg','\n\r300ml ½ pt full-cream milk.\n\r6 medium organic egg yolks.\n\r150g 5oz caster sugar.\n\r1 tsp vanilla extract.\n\r350ml 12fl oz double cream.\n\r30g 1¼ oz dark chocolate about 70 per cent cocoa','\n\r1.Pour the milk into a small pan and bring to the boil. Whisk the egg yolks and sugar in a bowl, then whisk in the milk. Return the mixture to the pan and heat gently until you have a thin pouring custard that coats the back of the spoon, taking care not to overheat it. Pour it straight away through a sieve into a clean bowl, stir in the vanilla, cover the surface with clingfilm and leave to cool, then chill.\n\r2.Using an electric whisk, whip the double cream until it forms soft fluffy peaks and fold it into the custard. Coarsely grate the chocolate.\n\r3.If using an ice-cream maker, freeze according to manufacturer’s instructions, scattering over the grated chocolate a few minutes before the end. Eat straight away, or transfer to a container, cover and freeze until required.\n\r4.Alternatively, use a food processor. First pour the ice-cream mixture into a container, seal and freeze until softly frozen (start checking after about 3 hours, and hourly thereafter). Scoop the soft ice cream into the bowl of a food processor and whiz to a thick slush. Spoon back into the container, scatter over and fold in the grated chocolate, seal and return it to the freezer for another few hours or overnight.','Bruce Weinstein',75,'2017-05-29',1),(9,'Chocolate ice cream','images/Chocolate ice cream(300x300).jpg','\n\r1 1/2 cups heavy cream.\n\r1 1/2 cups whole milk.\n\r1/2 cup unsweetened cocoa powder.\n\r4 ounces semisweet chocolate, finely chopped.\n\r4 large egg yolks.\n\r3/4 cup granulated sugar.\n\r1 teaspoon vanilla extract','\n\r1.Prepare an ice water bath by filling a large bowl halfway with ice and water; set aside. Set a fine-mesh strainer over a large heatproof bowl and set aside.\n\r2.Combine cream and milk in a large saucepan and bring to a simmer over medium heat. Remove from heat and whisk in cocoa powder until incorporated. Add chocolate and whisk until completely melted and smooth; set aside.\n\r3.Whisk egg yolks in a large bowl until smooth. Gradually add sugar and whisk until mixture is pale yellow and thickened, about 3 minutes. Slowly pour about a quarter of the chocolate mixture into the egg mixture, whisking constantly so the hot mixture doesn’t scramble the eggs.\n\r4.Pour chocolate-egg mixture back into the saucepan. Cook over low heat, stirring constantly with a wooden spoon, until the mixture thickens slightly and coats the spoon, about 5 minutes. (When you draw your finger across the spoon, it should make a mark through the mixture, which should not run back in on itself).\n\r5.Remove from heat and strain through the prepared fine-mesh strainer. Stir vanilla extract into the chocolate base, then set the bowl over the ice bath to cool to room temperature, about 15 to 20 minutes.\n\r6.Remove the chocolate base from the ice bath, cover, and place in the refrigerator until completely chilled, at least 3 hours or overnight. Freeze in an ice cream maker according to the manufacturer’s instructions. The ice cream will keep in an airtight container in the freezer for up to 1 week','Bruce Weinstein',20,'2017-05-29',1),(10,'Chocolate truffle ice cream','images/Chocolate truffle ice cream(300x300).jpg','\n\r1/2 cup sugar.\n\r2 large egg yolks.\n\r1 cup milk.\n\r1/4 cup cocoa powder.\n\r1 1/2 cups heavy cream.\n\r6 ounces bittersweet chocolate, chopped.\n\r1 teaspoon vanilla extract','\n\r1.In a small mixing bowl, beat the sugar into the egg yolks until thickened and pale yellow. Set aside.\n\r2.Bring the milk to a simmer in a heavy medium saucepan. Whisk in the cocoa and bring the milk back to a simmer. Simmer 3 minutes, stirring constantly. Slowly beat the hot milk and cocoa into the eggs and sugar. Pour the entire mixture back into the pan and place over low heat. Stir constantly with a whisk or wooden spoon until the custard thickens slightly. Be careful not to let the mixture boil or the eggs will scramble. Remove from the heat and pour the hot chocolate custard through a strainer into a large, clean bowl. Set aside while you prepare the ganache.\n\r3.Bring the cream to a simmer in a small saucepan. Immediately remove from the heat and pour the cream over the chopped chocolate in a bowl. Stir until the chocolate is melted and the mixture is smooth. Combine the chocolate mixtures, then stir in the vanilla. Cover and refrigerate until cold or overnight.\n\r4.Stir the chilled custard, then freeze in 1 or 2 batches in your ice cream machine according to the manufacturers instructions. When finished, the ice cream will be soft but ready to eat. For firmer ice cream, transfer to a freezer-safe container and freeze at least 2 hours.','Bruce Weinstein',100,'2017-05-29',1),(11,'Coffee ice cream','images/Coffee ice cream(300x300).jpg','\n\r1 1/2 cups whole milk.\n\r3/4 cup sugar.\n\r1 1/2 cups whole coffee beans (decaf unless you want the caffeine in your ice cream).Pinch of salt.\n\r1 1/2 cups heavy cream.\n\r5 large egg yolks.\n\r1/4 teaspoon vanilla extract.\n\r1/4 teaspoon finely ground coffee (press grinds through a fine mesh sieve)','\n\r1 Heat the milk, sugar, whole coffee beans, salt, and 1/2 cup of the cream in a medium saucepan until it is quite warm and steamy, but not boiling. Once the mixture is warm, cover, remove from the heat, and let steep at room temperature for 1 hour.\n\r2 Pour the remaining 1 cup of cream into a medium size metal bowl, set on ice over a larger bowl. Set a mesh strainer on top of the bowls. Set aside.\n\r3 Reheat the milk and coffee mixture, on medium heat, until again hot and steamy (not boiling!). In a separate bowl, whisk the egg yolks together. Slowly pour the heated milk and coffee mixture into the egg yolks, whisking constantly so that the egg yolks are tempered by the warm milk, but not cooked by it. Scrape the warmed egg yolks back into the saucepan.\n\r4 Stir the mixture constantly over medium heat with a heatproof, flat-bottomed spatula, scraping the bottom as you stir, until the mixture thickens and coats the spatula so that you can run your finger across the coating and have the coating not run. This can take about 10 minutes.\n\r5 Pour the custard through the strainer and stir it into the cream. Press on the coffee beans in the strainer to extract as much of the coffee flavor as possible. Then discard the beans. Mix in the vanilla and finely ground coffee, and stir until cool.\n\r6 Chill the mixture thoroughly in the refrigerator, then freeze it in your ice cream maker according to the manufacturers instructions.','Bruce Weinstein',1,'2017-05-29',1),(12,'Fruit and nut ice cream','images/Fruit and nut ice cream(300x300).jpg','\n\r55g (1/3 cup) hazelnuts, cut in half.\n\r25g (1/4 cup) flaked almonds.\n\r80g dried peaches, chopped.\n\r60g dried whole dessert figs, chopped.\n\r55g (1/4 cup) pistachio kernels.\n\r1L container Connoisseur Super Premium.\n\rClassic Vanilla ice-cream','\n\rStep 1: Preheat oven to 180°C. Line an 11 x 21cm (base measurement) loaf pan with 2 layers of plastic wrap, allowing it to overhang.\n\rStep 2: Spread the hazelnuts and almonds over a baking tray and cook in preheated oven for 3-4 minutes or until light golden. Place the hazelnuts in a tea towel and rub to remove the skin. Transfer the hazelnuts and almonds to a medium bowl. Add the peaches, figs and pistachio kernels, and stir until well combined.\n\rStep 3: Sprinkle the fruit and nut mixture over the base of the prepared pan. Spoon the ice-cream over the nut mixture and use the back of a spoon to smooth the surface. Cover with a double layer of plastic wrap and place in freezer for 6 hours or overnight until firm.\n\rStep 4: To make the toffee cream, place the sugar in a medium frying pan over low heat and cook, stirring, for 3 minutes or until it turns golden brown. Remove pan from heat and gradually stir in the cream (the toffee will harden). Place pan over low heat and cook, stirring, for 2-3 minutes or until toffee dissolves and sauce is smooth. Remove from heat and set aside for 5 minutes to cool slightly.\n\rStep 5: Turn ice-cream onto a serving platter. Remove plastic wrap and cut into 2.5cm-thick slices. Serve immediately with the toffee cream.','Bruce Weinstein',1,'2017-05-29',1),(13,'Kiwi fruit ice cream','images/Kiwi fruit ice cream(300x300).jpg','\n\r568ml carton double cream.\n\r300ml whole milk.\n\r6 large egg yolks, plus 1 egg white.\n\r100g caster sugar.\n\rGrated zest of 1 lemon.\n\r6 large ripe kiwi fruits.\n\rIcing sugar to taste','\n\r1.Heat the cream and milk together in a heavy-based pan to just below boiling point. Meanwhile, beat the egg yolks and sugar together in a bowl until thick and creamy. Pour the hot cream mixture on to the egg yolk mixture, whisking constantly. Return the mixture to the pan and heat gently, stirring, until the custard thickens enough to lightly coat the back of a wooden spoon - 10-12 minutes. Strain into a clean bowl, add the lemon zest and leave to cool. At this stage, you can chill the custard for a few hours before continuing.\n\r2.Top and tail the kiwi fruits and remove the skin. Cut the fruit into quarters, remove the white centre then purée the fruit in a blender. Make a thick pad out of layers of kitchen paper. Pour the purée on to the paper and leave for 2-3 minutes to absorb the water from the fruit.\n\r3.Mix the kiwi purée into the custard. If the mix tastes tart, sweeten with a little icing sugar (the ice cream will taste even less sweet when frozen).\n\r4.Process the custard in an ice cream maker until thick and holding its shape. Transfer to a freezer container. Alternatively, pour the custard into a freezer container, cover, freeze for 2 hours and whisk well to break up the ice crystals. Return to the freezer. Repeat the twice more or until the ice cream is very thick and holds its shape.\n\r5.Whisk the egg white until it holds soft peaks. Fold into the thick ice cream. Freeze overnight or until firm.\n\r6.Remove from the freezer 5 minutes before serving.','Bruce Weinstein',1,'2017-05-29',1),(14,'Mango ice cream','images/Mango ice cream(300x300).jpg','\n\r1 ripe mango, medium to large.\n\r2 eggs.\n\r1 cup sugar.\n\r1 tsp. vanilla.\n\r2 cups cream.\n\r1 to 2 cups milk','\n\r1.Peel and slice the mango, choosing a less stringy type of mango, if possible. Place in blender and puree until very smooth. If you start with about 2 cups of pieces, you should have 1 to 1 1/2 cups of puree. If your mango is very large, chop the excess and use to top the ice cream later.\n\r2.Beat the eggs with a wire whisk for 2 minutes until thick and lemon-colored. Slowly whisk in the sugar, beating about a minute more. Stir in the vanilla, cream, and mango puree. Add enough milk to fill your container to the proper level. For the Cuisinart ice cream maker, 5 to 5 1/4 cups is the maximum.\n\r3.Chill the mixture very well. Freeze according to your ice cream maker instructions.\n\r4.If you must, substitute low-fat, no-fat, fake egg, sugar-free stuff, but I take no responsibility for that!','Bruce Weinstein',1,'2017-05-29',1),(15,'Pineapple ice cream','images/Pineapple ice cream(300x300).jpg','\n\r2 cups milk.\n\r1 cup sugar.\n\r3 eggs, lightly beaten.\n\r1-3/4 cups heavy whipping cream.\n\r1 can (8 ounces) crushed pineapple, undrained','\n\r1.In a large saucepan, heat milk to 175°; stir in sugar until dissolved. Whisk a small amount of the hot mixture into the eggs. Return all to the pan, whisking constantly. Cook and stir over low heat until mixture reaches at least 160° and coats the back of a metal spoon.\n\r2.Remove from the heat. Cool quickly by placing pan in a bowl of ice water; stir for 2 minutes. Stir in whipping cream and pineapple. Press waxed paper onto surface of custard. Refrigerate for several hours or overnight.\n\r3.Fill cylinder of ice cream freezer two-thirds full; freeze according to the manufacturer’s directions. Refrigerate remaining mixture until ready to freeze. When ice cream is frozen, transfer to a freezer container; freeze for 2-4 hours before serving.','Bruce Weinstein',1,'2017-05-29',1),(16,'Pistachio ice cream','images/Pistachio ice cream(300x300).jpg','\n\r1 C crushed butter-flavored crackers.\n\r1/4 C butter, melted.\n\r3/4 C cold milk.\n\r1 pkg (3.4-oz) instant pistachio pudding mix.\n\r1 qt. vanilla ice cream, softened.\n\r1 carton (8-oz) frozen whipped topping, thawed.\n\r2 pkg (1.4-oz each) Heath candy bars, crushed','\n\r1.In bowl, combine cracker crumbs and butter. Press into an ungreased 9-in. square baking pan. Bake at 325 degrees for 7-10 minutes or until lightly browned. Cool on a wire rack.\n\r2.Meanwhile, in a large bowl, whisk milk and pudding mix for 2 minutes. Let stand for 2 minutes or until soft-set. Stir in ice cream; pour over crust. Cover and freeze for 2 hours or until firm.\n\r3.Spread with whipped topping; sprinkle with crushed candy bars. Cover and freeze for 1 hour or until firm.','Bruce Weinstein',1,'2017-05-29',1),(17,'Strawberry ice cream','images/Strawberry ice cream(300x300).jpg','\n\r175 gram(s) caster sugar (plus 2 tablespoons).\n\r500 ml Full fat milk.\n\r500 ml double cream.\n\r1 vanilla pod.\n\r2 tablespoon(s) lemon juice.\n\r10 Egg yolks.\n\r500 gram(s) strawberries','\n\r1.Hull and roughly chop the strawberries, put them into a bowl and sprinkle over the 2 tablespoons of caster sugar and leave them to steep and infuse with flavour.\n\r2.Pour the milk and cream into a heavy-based saucepan, and add the vanilla pod, split down the middle lengthways. Bring the pan nearly to the boil and then take it off the heat and leave to infuse for 20 minutes.\n\r3.In a large bowl whisk the egg yolks and the 175g sugar until thick and pale yellow. Take the vanilla pod out of the milk and cream and pour, whisking the while, the warm liquid over the yolks. Put the cleaned-out pan back on the heat with the cream, milk, egg and sugar mixture and stir the custard until it thickens, then take it off the heat and pour it into a bowl to cool.\n\r4.Puree the strawberries in a processor, and when the custard is cool fold in the lemon juice and strawberry puree.\n\r5.At this point you can either freeze the ice cream in an ice-cream maker, or in a plastic tub in the freezer. If you do the latter you should whip it out every hour for 3 hours as it freezes and give it a good beating, either with an electric whisk, by hand or in the processer. That gets rid of any ice crystals that form and that make the ice cream crunchy rather than smooth.','Bruce Weinstein',1,'2017-05-29',1),(18,'Vanilla and strawberry ice cream','images/Vanilla and strawberry ice cream(300x300).jpg','\n\r3/4 Cup sugar.\n\r1 Cup whole milk (raw if possible).\n\r1/4 t. salt.\n\r1 vanilla bean, split and scraped.\n\r3 egg yolk, lightly beaten.\n\r2 Cup heavy cream.\n\r1 Cup finely diced fresh strawberries.\n\r1/4 Cup sugar','\n\r1.Combine sugar, milk, salt, and scraped vanilla bean and pods in a saucepan over low heat.  Stir until the mixture just begins to steam and simmer.   Place the egg yolks into a small bowl. Gradually stir in about 1/2 cup of the hot liquid to temper the eggs and return everything to the saucepan. Heat until thickened, about 5 minutes, but be careful not to boil. Remove from the heat, and pour into a chilled bowl and refrigerate for at least two hours or overnight.\n\r2.Using a slotted spoon, fish out the vanilla pods from the chilled custard.Whip the heavy cream until it forms soft peaks and gently fold into the custard mixture. Pour into an ice cream maker, and freeze according to manufacturer’s directions.  Meanwhile, combine the diced strawberries with the remaining sugar and set aside.\n\r3.When ice cream is done churning, scoop out into a container with a lid.  Fold in the strawberries while ice cream is still soft.  Cover container tightly and place in freezer to firm up, about 3-4 hours. ','Bruce Weinstein',1,'2017-05-29',1),(19,'Vanilla ice cream','images/Vanilla ice cream(300x300).jpg','\n\r1 cup (250ml) whole milk.\n\rA pinch of salt.\n\r3/4 cup (150g) sugar.\n\r1 vanilla bean, split lengthwise.\n\r2 cups (500ml) heavy cream.\n\r5 large egg yolks.\n\r1 teaspoon pure vanilla extract','\n\r1. Heat the milk, salt, and sugar in a saucepan. Scrape the seeds from the vanilla bean into the milk with a paring knife, then add the bean pod to the milk. Cover, remove from heat, and infuse for one hour.\n\r2. To make the ice cream, set up an ice bath by placing a 2-quart (2l) bowl in a larger bowl partially filled with ice and water. Set a strainer over the top of the smaller bowl and pour the cream into the bowl.\n\r3. In a separate bowl, stir together the egg yolks. Rewarm the milk then gradually pour some of the milk into the yolks, whisking constantly as you pour. Scrape the warmed yolks and milk back into the saucepan.\n\r4. Cook over low heat, stirring constantly and scraping the bottom with a heat-resistant spatula, until the custard thickens enough to coat the spatula.\n\r5. Strain the custard into the heavy cream. Stir over the ice until cool, add the vanilla extract, then refrigerate to chill thoroughly. Preferably overnight.\n\r6. Remove the vanilla bean and freeze the custard in your ice cream maker according to the manufacturer’s instructions.\n\rNote: Used vanilla beans can be rinsed and dried, then stored in a bin of sugar. That sugar can be used for baking and, of course, for future ice cream making.M: Soak 1/2 cup raisins in 1/2 cup rum for 30 minutes. When the ice cream is finished, add the raisins and rum and continue cranking until they are mixed in and the ice cream has refrozen. Blend in 1/3 cup chopped toasted walnuts with a spoon or spatula. ','Bruce Weinstein',1,'2017-05-29',1),(20,'Walnut ice cream','images/Walnut ice cream(300x300).jpg','\r\n\r\n1 quart milk.\r\n\r\n1 quart whipping cream.\r\n\r\n2 cups sugar, divided.\r\n\r\n2 vanilla beans, cut in half lengthwise.\r\n\r\n2 dozen egg yolks.\r\n\r\n2 cups coarsely chopped walnuts, toasted','\r\n\r\n1.Bring milk, whipping cream, 1 cup sugar, and vanilla beans to a boil in a large, heavy saucepan over medium heat, whisking constantly. Remove from heat.\r\n\r\n2.Whisk together egg yolks and remaining 1 cup sugar in a large bowl. Gradually add one-fourth hot mixture into yolk mixture; whisk back into remaining hot mixture.\r\n\r\n3.Cook over low heat, whisking constantly, 10 to 15 minutes or until mixture thickens.\r\n\r\n4.Pour mixture through a fine wire-mesh strainer into a large bowl, discarding vanilla beans. Immediately set bowl in ice, and let stand, stirring constantly, until cool.\r\n\r\n5.Stir in walnuts.\r\n\r\n6.Pour mixture into the freezer container of a 1-gallon electric freezer. Freeze according to manufacturers instructions.\r\n\r\n7.Pack freezer with additional ice and rock salt, and let stand 1 hour before serving.','Bruce Weinstein',1,'2017-05-29',0),(27,'hung','images/nobita-yellow.jpg','đá','ádsad','ádd',1,'2020-04-20',1);
/*!40000 ALTER TABLE `recipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reference`
--

DROP TABLE IF EXISTS `reference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reference` (
  `reference_id` int NOT NULL AUTO_INCREMENT,
  `monthly_fee` float NOT NULL,
  `yearly_fee` float NOT NULL,
  `book_cost` float NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`reference_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reference`
--

LOCK TABLES `reference` WRITE;
/*!40000 ALTER TABLE `reference` DISABLE KEYS */;
INSERT INTO `reference` VALUES (1,15,150,49.99,'The Ultimate Ice Cream Book contains enough recipes to fill your summer days with delicious frozen desserts -- but after acquainting yourself with this book\'s hundreds of tempting concoctions, you\'ll want to use it every day of the year. With over 500 recipes, author Bruce Weinstein has put together the most comprehensive cookbook of its kind, covering just about every conceivable flavor of ice cream, sorbet, and granita; dozens of different recipes for shakes, malts, and other cold drinks; how to make your own ice cream cones; and toppings galore. \\n If you ever worried that you might not get full use out of your ice-cream maker, cast your doubts aside. Ice cream recipes feature such unusual flavors as lavender, chestnut, rhubarb, and Earl Grey tea. Even Weinstein\'s vanilla ice cream is anything but plain, with variations like Vanilla Crunch, Vanilla Rose, and Vanilla Cracker Jack. There is also a plethora of light, refreshing recipes for sorbets and granitas, with flavors like Apple Chardonnay, Coconut, and Kiwi. Top everything off with the author\'s recipes forhomemade sauces. Whether it\'s a special event or a midnight snack, The Ultimate Ice Cream Book has what you need to make any occasion a little sweeter.');
/*!40000 ALTER TABLE `reference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_recipe`
--

DROP TABLE IF EXISTS `user_recipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_recipe` (
  `user_recipe_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `desciption` text NOT NULL,
  `details` text NOT NULL,
  `customer_id` int DEFAULT NULL,
  `prize_status` tinyint(1) NOT NULL DEFAULT '0',
  `enable_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_recipe_id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `user_recipe_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_recipe`
--

LOCK TABLES `user_recipe` WRITE;
/*!40000 ALTER TABLE `user_recipe` DISABLE KEYS */;
INSERT INTO `user_recipe` VALUES (1,'Ice Cream on Sticks','images/userrecipe/Ice Cream on Sticks.jpg','1 cup orange juice \n\rBowl of whipped cream\n\rSmall glasses\n\r5 ice cream sticks\n\rVanilla essence','This is a very simple recipe to make ice cream on sticks. I have taken orange juice here, you can go in for any flavor you like. It could be strawberry juice, pomegranate is a good option and so is cranberry. Whatever juice you decide; pour it in small glasses and place a stick in there. Refrigerate it until it is converted into ice. Now, to the bowl of whipped cream add a few drops of vanilla essence and beat it, until it develops a thick consistency. Pour it over the ice candy and enjoy it to the fullest. Ice candies would serve as a very good dessert idea for your kids birthday party as well. They are also a healthy option, as they have less fats compared to the normal ice cream.',1,1,1),(2,'Chocolate Soy Milk Ice Cream','images/userrecipe/Chocolate Soy Milk Ice Cream.jpg','Soy milk, 3 cups\n\rCocoa powder, 2 to 3 teaspoons\n\rPowdered sugar, 1 cup\n\rCornstarch, 1½ teaspoons\n\rVanilla bean or vanilla essence, 1 teaspoon\n\rEgg yolks','Take a large bowl and beat the egg yolks along with the cornstarch and powdered sugar. Heat the soy milk in a medium-sized saucepan, making sure that it does not boil. Add the cocoa powder to the milk and stir till it incorporates into the milk. Once the milk has warmed, pour this into the egg mixture, all the while, whisking it. Add the vanilla essence and continue to stir it. Now transfer the contents of the bowl to the same saucepan and warm it till the mixture becomes thick. Pour the mixture into a separate bowl and place it in a refrigerator for about a couple of hours to chill it. Pour this chilled mixture into a soy milk ice cream maker and process it. Scoop out the ice cream and serve it with freshly cut fruits, of your choice.',2,1,1),(3,'Ice Cream with Coconut','images/userrecipe/Ice Cream with Coconut.jpg','3 cups coconut milk\n\r3 tbsp honey\n\r2 cups blueberries','In a blender, blend blueberries till they are smooth.\n\rAdd coconut milk and honey to the blueberry puree.\n\rPour the ice cream mixture in a bowl and let it chill for about 2 to 4 hours.\n\rNow the mixture can be placed in an ice cream maker and follow the instructions on the ice cream maker to get smooth coconut milk ice cream.',3,1,1),(4,'Non Dairy Ice Cream','images/userrecipe/Non Dairy Ice Cream.jpg','5 cups soy milk or almond milk or coconut milk\n\r2 cup soy milk powder\n\r½ cup cocoa powder\n\r1 ½ cups sugar\n\r1 cup powdered sugar\n\r1 tbsp cider vinegar','Blend soy milk with soy milk powder and vinegar till all the ice cream ingredients are well mixed.\n\rPour the soy milk mixture in a saucepan and add sugar and powdered sugar to it, along with the cocoa powder. Let the mixture come to a boil over medium-low heat.\n\rWhen the mixture is coming to a boil, ensure you continue to stir constantly.\n\rLet the mixture cook till it is thick and syrupy. It should resemble a pudding.\n\rThen pour the mixture in a metal pan and place it in the freezer without covering it for about an hour.\n\rOnce the mixture thickens, remove the pan from the freezer and scrap it and place it in a blender.\n\rBlend the mixture on high for 30 to 40 seconds, so that the mixture is creamy.\n\rNow pour the mixture in a pan and place it back in the freezer.\n\rYou will have to repeat the procedure 3 times after every 30 to 45 minutes, after which let the ice cream freeze for an hour and half before you serve it.\n\rYou can add toppings of your choice, if you want to, on this ice cream.',1,0,1),(5,'Almond Milk Ice Cream','images/userrecipe/Almond Milk Ice Cream.jpg','Almond milk - 2.5 cups\n\rEgg yolks - 8\n\rSugar - 1.25 cups\n\rVanilla extract - 2 teaspoons\n\rHeavy cream - 1.5 cups','Start with heating almond milk and heavy cream in a sauce pan placed over medium heat. Once the mixture boils, cook at low temperature for a few minutes and remove from heat. In another bowl, beat the egg yolks and when it is half done, add sugar and whisk thoroughly. Once done, slowly add the almond milk-cream mixture to the egg-sugar mixture in small amounts. Make sure to stir constantly, so that the eggs do not get cooked with the hot milk. Once the whole milk is added, transfer the contents of the bowl to the sauce pan. Once again, place it on medium heat and cook till a temperature of 175° F is reached. Remove from heat and place the saucepan in an icebox so as to cool the contents as soon as possible. Add the vanilla extract and allow the mixture to cool below 40 degree Fahrenheit. If you have an almond milk ice cream maker, you can use it to churn this mixture. Otherwise, use the ice cream mixing bowl of the mixer and run it for at least 30 minutes. Once done, freeze this mixture, before serving, so that it gets the consistency of regular ice cream.',2,0,1),(6,'Low-carb Ice Cream','images/userrecipe/Low-carb Ice Cream.jpg','Vanilla essence, 3 tsp.\n\rAny commercial artificial sweetener, 10 tsp.\n\rUnflavored gelatin, 2 tbsp.\n\rAny commercial egg replacer, 8 oz\n\rCold skim milk, ½ cup\n\rFat free powdered milk, 2 cups','Take a dry saucepan and mix the powdered milk and gelatin together. Place over a low flame and stir gently and continuously till the gelatin is completely dissolved and you get an even mixture.\n\rRemove the saucepan from the flame and set aside to allow it to cool down to room temperature.\n\rBlend all the remaining ingredients in a food processor till they are evenly combined and the mixture has a fine texture.\n\rOnce the milk-gelatin mixture has cooled down sufficiently, add it to the other ingredients and blend again for a couple of minutes.\n\rRefrigerate the entire mixture till its just about to freeze. Blend for a few seconds.\n\rPass it through a strainer into the ice cream maker and freeze as instructed therein.',3,0,1),(7,'Sugar-free Ice Cream','images/userrecipe/Sugar-free Ice Cream.jpg','Gelatin\n\rLow fat milk\n\rSugar sweeteners\n\rChocolate drink mix or cocoa powder (you can use flavor or essence of your choice say vanilla or strawberry or mango etc.)\n\rSalt\n\rBlender\n\rSaucepans\n\rBowls\n\rEggs (optional)','Add gelatin to half a cup of low fat milk in a saucepan and heat until the gelatin softens and is fully mixed.\n\rKeep the mixture in cold water in a bowl, until it cools down.\n\rAdd rest of the milk, sugar sweetener, ice cream essence and flavor and a pinch of salt to the mixture and blend it.\n\rChill the mixture in refrigerator and then add in the ice cream maker. Make sure that the blending is proper; its the most important step in ice cream making.\n\rFreeze until the ice cream is set properly. Follow the manufacturers instructions for the same.\n\rYour sugar-free ice cream is ready to eat!\n\rWhile for eggs, beat them properly and then add during blending.',1,0,1),(8,'Fried Ice Cream','images/userrecipe/Fried Ice Cream.jpg','Vanilla Ice Cream 1 quart\n\rCrushed Frosted Cornflakes 1 cup\n\rCookie Crumbs 1 cup\n\rEggs 2\n\rSugar 2 tablespoons\n\rVegetable Oil for frying\n\rHot Chocolate Sauce\n\rWhipped Cream','With the ice cream, form 4 large balls of ice cream, wrap them in waxed paper and cover with plastic wrap to freeze them for at least 2 to 3 hours.\n\rIn a bowl, combine cornflakes and cookie crumbs and spread the mixture in a shallow dish.\n\rDip the ice cream balls in the cornflakes mixture and freeze again for 30 minutes.\n\rBeat eggs and sugar in a bowl.\n\rDip the ice cream balls in the eggs and roll them again in the crumb mixture and make sure that the balls are well coated.\n\rYou will have to freeze the ice cream balls again for an hour or longer.\n\rHeat oil to 400 degrees Fahrenheit and lower the balls into the oil and fry them till the balls are golden brown in color.\n\rRemove the ice cream balls and place them on a dessert bowl, drizzle some chocolate sauce and whipped cream on it and serve.',2,0,1),(9,'Cupcake Ice Cream','images/userrecipe/Cupcake Ice Cream.jpg','2 cups self-raising flour\n\r1 cup sugar\n\r½ cup unsalted butter\n\r3 eggs\n\r1 teaspoon baking powder\n\r1 teaspoon vanilla extract\n\r½ cup milk\n\r½ teaspoon salt\n\rAssorted candy sprinkles\n\r¼ cup chopped roasted pistachios\n\r12 cherries\n\r1 cup whipping cream\n\r½ cup icing sugar\n\rFood color (optional)\n\r12 flat bottomed ice cream cones','In a large bowl, sift the self-raising flour with the baking powder and the salt. In another bowl whisk the eggs, milk, sugar and butter. Pour this mixture in the bowl containing the dry ingredients. Whisk all this together along with a tablespoon of vanilla extract until everything is well blended. Now when your cupcake batter is ready, fill each cone with 2 to 3 tablespoons of this batter. Bake in an oven at 220º centigrade for 20 to 25 minutes. Remove from the oven and let it cool completely.\n\rBeat the whipping cream until stiff peaks are formed. Add in the icing sugar and whip this mixture until all the sugar has been dissolved. Put half a tablespoon of food coloring if you wish. Fill a pastry bag with this icing and pipe it on top of the cupcake cones. Decorate the ice cream cones with assorted candy sprinkles, chopped roasted pistachios and cherries.',3,0,1),(10,'Classic Chocolate Ice Cream','images/userrecipe/Classic Chocolate Ice Cream.jpg','Milk, 1 cup\r\n\r\nGranulated sugar, ½ cup\r\n\r\nBittersweet/semi-sweet chocolate,1⅓ cup\r\n\r\nHeavy cream, 2 cups\r\n\r\nVanilla extract,1 tsp','Heat milk in a stove or microwave until it just begins to bubble. Meanwhile, process chocolate in a food processor or blender until the chocolate is finely chopped. Mix chocolate and sugar. Combine hot milk and chocolate mixture and blend until chocolate is melted and the mixture is smooth. Cool the mixture. Once you have done that, stir in heavy cream and vanilla. Chill for at least 30 minutes. Pour mixture into your ice cream maker. Mix in ice cream machine for 25 - 30 minutes or according to manufacturers instructions.',1,0,1),(32,'<p>','images/uploaded/noavatar.gif','package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}','package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}package com.zetcode;\r\n\r\nimport org.springframework.boot.SpringApplication;\r\nimport org.springframework.boot.autoconfigure.SpringBootApplication;\r\n\r\n@SpringBootApplication\r\npublic class Application  {\r\n    \r\n    public static void main(String[] args) {\r\n        SpringApplication.run(Application.class, args);\r\n    }\r\n}',NULL,1,1);
/*!40000 ALTER TABLE `user_recipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_role` (
  `ID` bigint NOT NULL,
  `customer_id` bigint NOT NULL,
  `ROLE_ID` bigint NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ROLE_FK2` (`ROLE_ID`),
  CONSTRAINT `USER_ROLE_FK2` FOREIGN KEY (`ROLE_ID`) REFERENCES `app_role` (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,2,1),(2,18,2),(3,2,2);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-14 21:48:51
