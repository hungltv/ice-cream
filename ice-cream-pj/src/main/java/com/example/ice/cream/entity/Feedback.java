package com.example.ice.cream.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Feedback {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "feedback_id", updatable = false, nullable = false)
	private Long feedback_id;
	
	private String full_name;
	private String title;
	private String content;

	
	public Feedback() {}

	

	public Feedback(Long feedback_id, String full_name, String title, String content) {
		super();
		this.feedback_id = feedback_id;
		this.full_name = full_name;
		this.title = title;
		this.content = content;
	}



	public Long getFeedback_id() {
		return feedback_id;
	}


	public void setFeedback_id(Long feedback_id) {
		this.feedback_id = feedback_id;
	}


	public String getFull_name() {
		return full_name;
	}


	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}

	
}
