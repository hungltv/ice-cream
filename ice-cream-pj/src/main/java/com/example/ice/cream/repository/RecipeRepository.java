package com.example.ice.cream.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.example.ice.cream.entity.Recipe;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {

	@Query(value = "select * from Recipe re ORDER BY re.recipe_id ASC limit :limit", nativeQuery = true)
	List<Recipe> findAll(@Param("limit") int limit);
	
	@Query(value = "select * from Recipe re ORDER BY re.recipe_id DESC limit :limit", nativeQuery = true)
	List<Recipe> findAllDesc(@Param("limit") int limit);
	
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE Recipe re SET"
			+ " re.name = :name,"
			+ " re.desciption = :desciption,"
			+ " re.details = :details,"
			+ " re.author = :author,"
			+ " re.image = :image,"
			+ " re.enable_status = :enable_status "
			+ " WHERE re.recipe_id = :recipe_id", nativeQuery = true)
	int updateRecipe(
			@Param("recipe_id") Long recipe_id,
			@Param("name") String name,
			@Param("desciption") String desciption,
			@Param("details") String details,
			@Param("author") String author,
			@Param("image") String image,
			@Param("enable_status") int enable_status);
	
}
