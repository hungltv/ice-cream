package com.example.ice.cream.entity;

import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "customer_id", updatable = false, nullable = false)
	private Integer customer_id;
	
	@NotNull(message="username must not be null")
	@Size(min = 10, max = 100, message="username must be between 10 and 100 characters")
	String username;
	
	@NotNull(message="password must not be null")
	@Size(min = 6, max = 100, message="username must be between 6 and 100 characters")
	private String password;
	
	@NotNull(message="Full name must not be null")
	@Size(min = 1, max = 100, message="Full name must be between 1 and 100 characters")
	private String full_name;
	
	private String address;
	private String phone_number;
	
	@Email(message="email not valid")
	private String email;
	private String gender;
	private String birthday;
	private String avatar;
	
//	@NotNull(message="expired date must not be null")
	private String expired_date;
	
	private Integer enable_status;

	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<UserRole> user_role;

	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
	private Set<UserRecipe> user_recipe;
	
	
	public Set<UserRecipe> getUser_recipe() {
		return user_recipe;
	}

	public void setUser_recipe(Set<UserRecipe> user_recipe) {
		this.user_recipe = user_recipe;
	}

	public Customer() {}
	
	public Customer(Integer customer_id, String username, String password, String full_name, String address,
			String phone_number, String email, String gender, String birthday, String avatar, String expired_date,
			Integer enable_status) {
		super();
		this.customer_id = customer_id;
		this.username = username;
		this.password = password;
		this.full_name = full_name;
		this.address = address;
		this.phone_number = phone_number;
		this.email = email;
		this.gender = gender;
		this.birthday = birthday;
		this.avatar = avatar;
		this.expired_date = expired_date;
		this.enable_status = enable_status;
	}

	public Integer getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(Integer customer_id) {
		this.customer_id = customer_id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFull_name() {
		return full_name;
	}
	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getExpired_date() {
		return expired_date;
	}
	public void setExpired_date(String expired_date) {
		this.expired_date = expired_date;
	}
	public Integer getEnable_status() {
		return enable_status;
	}
	public void setEnable_status(Integer enable_status) {
		this.enable_status = enable_status;
	}
	public Set<UserRole> getUser_role() {
		return user_role;
	}
	public void setUser_role(Set<UserRole> user_role) {
		this.user_role = user_role;
	}
}
