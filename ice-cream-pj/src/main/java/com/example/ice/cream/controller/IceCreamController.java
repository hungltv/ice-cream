package com.example.ice.cream.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.example.ice.cream.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.example.ice.cream.service.IceCreamService;

@RestController
public class IceCreamController {

	
	private IceCreamService service;
	
	@Autowired
	public void setService(IceCreamService service) {
		this.service = service;
	}
	private static final String UPLOADED_FOLDER = "./src/main/resources/static/images/uploaded/";
//	private static final String USER_RECIPE_FOLDER = "./src/main/resources/static/images/userrecipe/";
	private static final String IMAGES_FOLDER = "./src/main/resources/static/images/";

	private static final Logger LOGGER = LoggerFactory.getLogger(IceCreamController.class);
	@GetMapping("/user-recipe")
	public ResponseEntity<Optional<Recipe>> response(){
//		List<User_recipe> list = service.listUserRecipe();
		Optional<Recipe> objectDetail = service.findById(Long.valueOf(2));
		return new ResponseEntity<Optional<Recipe>>(objectDetail, HttpStatus.OK);
	}

	@GetMapping("/home")
	public ModelAndView home(HttpServletRequest request) {
		
		List<UserRecipe> lstRecipeHaveCustomer = service.fetchUserRecipDesc();

//		List<User_recipe> list = service.listUserRecipe();
		request.setAttribute("userRecipe", lstRecipeHaveCustomer);
		List<Recipe> lstRecipe = service.listRecipe(20);
		List<Recipe> lstRecipetop10 = new LinkedList<>();
		List<Recipe> lstRecipetop6 = new LinkedList<>();

		
		lstRecipe.forEach((recipe)->{			
			if(lstRecipetop10.size() >= 10) {
				
			} else {
				lstRecipetop10.add(recipe);
			}
			
			if(lstRecipetop6.size() >= 6) {
				
			} else {
				lstRecipetop6.add(recipe);
			}
			
		});
		request.setAttribute("lstRecipetop10", lstRecipetop10);
		request.setAttribute("lstRecipetop6", lstRecipetop6);

		return new ModelAndView("/home");
	}
	
	@GetMapping("/recipes/{id}")
	public ModelAndView recipes(@PathVariable Long id, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView("/recipes");
		Optional<Recipe> detailById = service.findById(id);
		Recipe recipe = detailById.get();
		
		request.setAttribute("view_number", recipe.getView_number());
		request.setAttribute("image", recipe.getImage());
		request.setAttribute("desciption", recipe.getDesciption());
		request.setAttribute("details", recipe.getDetails());
		
		List<Recipe> lstRecipe = service.listRecipe(15);
		request.setAttribute("lstRecipe", lstRecipe);
		List<Recipe> lstRecipetop5 = service.listRecipe(4);
		request.setAttribute("lstRecipetop5", lstRecipetop5);
		return mav;
	}
	
	@GetMapping("/messageOrderBook")
	public ModelAndView messageOrderBook(HttpServletRequest request, Model model) {
		
		return new ModelAndView("/messageOrderBook");
	}
	
	@GetMapping("/order-book")
	public ModelAndView orderBook(HttpServletRequest request, Model model) {
		ModelAndView mav = new ModelAndView("/orderBook");
		Optional<Reference> bookData = service.referenceFirst(1);
		Reference reference = new Reference();
		reference = bookData.get();
		request.setAttribute("bookCost", reference.getBook_cost());
		request.setAttribute("description", reference.getDescription());
		OnlineOrder orderbook = new OnlineOrder();
		model.addAttribute("online_order", orderbook);

		return mav;
	}
	
	@PostMapping("/sav-order-book")
	public ModelAndView savOrderBook(
			@Param("cardnumber") String cardnumber, 
			@Param("cardname") String cardname,
			@Param("ccv") String ccv,
			HttpServletRequest request,
			@Valid OnlineOrder online_order,
			BindingResult bindingResult, 
			Model model
			){

		if (bindingResult.hasErrors()) {
			Optional<Reference> bookData = service.referenceFirst(1);
			Reference reference = new Reference();
			reference = bookData.get();
			model.addAttribute("bookCost", reference.getBook_cost());
			model.addAttribute("description", reference.getDescription());
			model.addAttribute("cardnumber", cardnumber);
			model.addAttribute("cardname", cardname);
			model.addAttribute("ccv", ccv);

			return new ModelAndView("/orderBook");
		}
		
		online_order.setBook_cost(49.99);
		online_order.setStatus(1);
		OnlineOrder resp = service.saveOrderBook(online_order);
		int id = resp.getOrder_id();
		if(id > 0) {
			request.setAttribute("message", "your order has been\r\n" + 
					"successful");
		} else {
			request.setAttribute("message", "Please try again later!");
		}
		
		return new ModelAndView("redirect:/messageOrderBook");
	}
	
	@GetMapping("/get-profile/{customer_id}")
	public ModelAndView getProfile(@PathVariable Integer customer_id, HttpServletRequest request, Model model) {
		
		Optional<Customer> resp = service.findCustomerById(customer_id);
		Customer customer = resp.get();
		
		LocalDate today = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		LocalDate expired_Date = LocalDate.parse(customer.getExpired_date(), formatter);
		
		Duration diff = Duration.between(today.atStartOfDay(), expired_Date.atStartOfDay());
		long diffDays = diff.toDays() < 0 ? 0 : diff.toDays();
		
		model.addAttribute("dateUseService", diffDays);
		
		model.addAttribute("avatar", customer.getAvatar());
		model.addAttribute("customer", customer);
		model.addAttribute("myUploadForm", new MyUploadForm());
		return new ModelAndView("/update-profile");
	}
	
	
	@PostMapping("/update-profile")
	public ModelAndView updateProfile(
			HttpServletRequest request, @Valid Customer customer,
			BindingResult bindingResult, Model model, MyUploadForm myUploadForm
			) {
		
		model.addAttribute("customer", customer);
		model.addAttribute("myUploadForm", myUploadForm);
		
	    boolean ret = uploadFile(myUploadForm, model, IMAGES_FOLDER);
		if(ret) {
			customer.setAvatar("images/" + model.getAttribute("fileName"));
		}
		
		if (bindingResult.hasErrors()) {
			return new ModelAndView("/update-profile");
		}

		customer.setEnable_status(1);
		try {
			service.updateCustomer(customer);
		} catch (DataAccessException ex) {
			model.addAttribute("result", ex.getCause().getMessage());
        	LOGGER.error(ex.getCause().getMessage());
		}
    	try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/login-profile");
	}
	
	
	@GetMapping("/register")
	public ModelAndView register(Model model) {
		MyUploadForm myUploadForm = new MyUploadForm();
	    model.addAttribute("myUploadForm", myUploadForm);
		Optional<UserRole> res = service.findUserRole(1L);

		model.addAttribute("customer", new Customer());
		return new ModelAndView("/register");
	}
	
	@PostMapping("/register")
	public ModelAndView savRegister(
			HttpServletRequest request,  @Valid Customer customer, 
			BindingResult bindingResult, Model model, MyUploadForm myUploadForm
			) {
		LocalDate date = LocalDate.now();
		String expired_date = date.format(DateTimeFormatter.ISO_DATE);
	    model.addAttribute("myUploadForm", new MyUploadForm());
		model.addAttribute("customer", customer);
		
	    boolean ret = uploadFile(myUploadForm, model, IMAGES_FOLDER);
		if(ret) {
			customer.setAvatar("images/" + model.getAttribute("fileName"));
		} else {
			model.addAttribute("checkfile", model.getAttribute("message"));
			return new ModelAndView("/register");
		}
		
		if (bindingResult.hasErrors()) {
			return new ModelAndView("/register");
		}

		customer.setEnable_status(1);
		customer.setExpired_date(StringUtils.isEmpty(customer) ? null : expired_date);
		customer.setEmail(StringUtils.isEmpty(customer.getEmail()) ? null : customer.getEmail());
		customer.setBirthday(StringUtils.isEmpty(customer.getBirthday()) ? null : customer.getBirthday());
		
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String endcodepass = bCryptPasswordEncoder.encode(customer.getPassword());
		customer.setPassword(endcodepass);

        try {
			service.saveCustomer(customer);
			UserRole userRole = new UserRole();
			userRole.setCustomer(customer);
//			userRole.setRole_id(2);

			AppRole appRole = new AppRole();
			appRole.setRoleId(2L);
			userRole.setAppRole(appRole);
			service.saveRoleUser(userRole);
		} catch (DataAccessException ex) {
			model.addAttribute("result", ex.getCause().getMessage());
        	LOGGER.error(ex.getCause().getMessage());
		}
        
    	try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/login");
	}
	
//    @RequestMapping(value = "/uploadOneFile", method = RequestMethod.POST)
//    public ModelAndView uploadFile(MyUploadForm myUploadForm, Model model) {

//      upload ngoai source
//      try {
//        MultipartFile multipartFile = myUploadForm.getFileDatas();
//        String fileName = multipartFile.getOriginalFilename();
//        File file = new File(this.getFolderUpload(), fileName);
//        System.out.println(file);
//        multipartFile.transferTo(file);
//      } catch (Exception e) {
//        e.printStackTrace();
//        model.addAttribute("message", "Upload failed");
//      }
//    	
//        model.addAttribute("customer", new Customer());
//
//    	MultipartFile file = myUploadForm.getFileDatas();
//    	if (file.isEmpty()) {
//    		
//    		model.addAttribute("message", "please choose file");
//            return new ModelAndView("/register");
//        }
//        try {
//
//            // Get the file and save it somewhere
//            byte[] bytes = file.getBytes();
//            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
//            Files.write(path, bytes);
//            model.addAttribute("message",
//                    "You successfully uploaded '" + file.getOriginalFilename() + "'");
//            model.addAttribute("uploadedFiles", "images/" + file.getOriginalFilename());
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        
//      return new ModelAndView("/register");
//    }
    
//    public File getFolderUpload() {
//      File folderUpload = new File(System.getProperty("user.home") + "/OneDrive/Máy tính/GitLab/ice-cream/ice-cream-pj/src/main/resources/static/images");
//      if (!folderUpload.exists()) {
//        folderUpload.mkdirs();
//      }
//      return folderUpload;
//    }

	
	@GetMapping("/login")
	public ModelAndView login(HttpServletRequest request, 
			@Param("username") String username,
			@Param("password") String password) {		
		return new ModelAndView("/login");
	}
	
	@GetMapping("/")
	public ModelAndView loginư(HttpServletRequest request) {
		home(request);
		return new ModelAndView("/home");
	}
	

	
	@GetMapping("/login-profile")
	public ModelAndView loginProfile(HttpServletRequest request, Model model, Authentication authentication) {
		String username = authentication.getName();
//		Principal principal = request.getUserPrincipal();
//		String b = principal.getName();
		Customer customer = service.findCustomerByUsername(username);
		
		LocalDate today = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		LocalDate expired_Date = LocalDate.parse(customer.getExpired_date(), formatter);
		
		Duration diff = Duration.between(today.atStartOfDay(), expired_Date.atStartOfDay());
		long diffDays = diff.toDays() < 0 ? 0 : diff.toDays();
		
		model.addAttribute("dateUseService", diffDays);
		model.addAttribute("customer", customer);
		return new ModelAndView("/profile");
	}
	
	@GetMapping("/your-recipe/{customer_id}")
	public ModelAndView yourRecipe(@PathVariable("customer_id") Integer customer_id, HttpServletRequest request, Model model) {
		request.setAttribute("customer_id", customer_id);
		UserRecipe user_recipe = new UserRecipe();
		model.addAttribute("user_recipe", user_recipe);
	    model.addAttribute("myUploadForm", new MyUploadForm());
		return new ModelAndView("/your-recipe");
	}
	
	@PostMapping("/save-yourRecipe")
	public ModelAndView savYourrecipe(
			@Param("image") String image,
			@Param("desciption") String desciption,
			@Param("details") String details,
			@Param("customer_id") Integer customer_id,
			HttpServletRequest request,
			@Valid UserRecipe user_recipe,
			BindingResult bindingResult, MyUploadForm myUploadForm,
			Model model
			) {
		model.addAttribute("user_recipe", user_recipe);
	    model.addAttribute("myUploadForm", myUploadForm);
	    
		boolean ret = uploadFile(myUploadForm, model, IMAGES_FOLDER);
		if(ret) {
			user_recipe.setImage("images/" + model.getAttribute("fileName"));
		} else {
			user_recipe.setImage("images/uploaded/noavatar.gif");
		}
			
		if (bindingResult.hasErrors()) {
			return new ModelAndView("/your-recipe");
		}
		
		user_recipe.setDesciption(desciption);
		user_recipe.setDetails(details);
		user_recipe.setCustomer_id(customer_id);
		user_recipe.setEnable_status(1);
		user_recipe.setPrize_status(1);
		try {
			service.savUserRecipe(user_recipe);

		} catch (DataAccessException ex) {
			model.addAttribute("result", ex.getCause().getMessage());
        	LOGGER.error(ex.getCause().getMessage());
		}
        
    	try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		
		return new ModelAndView("redirect:/home");
	}
	
	@PostMapping("/cutomerChangePassword")
	public ModelAndView cutomerChangePassword(@Param("password") String password,
			@Param("customer_id") Integer customer_id,
			@Param("fullname") String fullname,
			HttpServletRequest request) {

		
		request.setAttribute("password", password);
		request.setAttribute("customer_id", customer_id);
		request.setAttribute("fullname", fullname);
		return new ModelAndView("/cutomerChangePassword");
	}
	
	@PostMapping("/changePassword")
	public ModelAndView changePassword(
			@Param("password") String password,
			@Param("customer_id") Integer customer_id,
			@Param("oldPassword") String oldPassword,
			@Param("newPassword") String newPassword,
			@Param("RePassword") String RePassword,
			HttpServletRequest request, Model model) {
		ModelAndView mav = null;
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		Customer customer = new Customer();
		boolean ret = checkPassword(oldPassword, password);
		
		if(!ret) {
			request.setAttribute("message", "Old password invalid !");
			request.setAttribute("password", password);

			mav = new ModelAndView("/cutomerChangePassword");
			return mav;
		}
		
		if(!newPassword.equals(RePassword)) {
			request.setAttribute("message", "Password do not match !");
			request.setAttribute("password", password);
			mav = new ModelAndView("/cutomerChangePassword");
			return mav;
		}
		
        String endcodepass = bCryptPasswordEncoder.encode(newPassword);
		customer.setPassword(endcodepass);
		
		customer.setCustomer_id(customer_id);
		try {
			service.changePassword(customer);

		} catch (DataAccessException ex) {
			model.addAttribute("result", ex.getCause().getMessage());
        	LOGGER.error(ex.getCause().getMessage());
		}
		mav = new ModelAndView("redirect:/login-profile");
		return mav;
	}
	
	public static boolean checkPassword(String password_plaintext, String stored_hash) {
		boolean password_verified = false;

		if(null == stored_hash || !stored_hash.startsWith("$2a$"))
			throw new java.lang.IllegalArgumentException("Invalid hash provided for comparison");

		password_verified = BCrypt.checkpw(password_plaintext, stored_hash);
		

		return(password_verified);
	}
	@GetMapping("/feedback")
	public ModelAndView feedback() {
		return new ModelAndView("/feedback");
	}
	
	@PostMapping("/feedback")
	public ModelAndView savFeedback(
			@Param("fullname") String fullname,
			@Param("title") String title,
			@Param("content") String content,
			HttpServletRequest request
			) {
		Feedback feedback = new Feedback();
		feedback.setFull_name(fullname);
		feedback.setTitle(title);
		feedback.setContent(content);
		service.savFeedback(feedback);
		request.setAttribute("message", "Thank you for your feedback !");
		return new ModelAndView("/feedback");
	}
	
	@GetMapping("/FAQ")
	public ModelAndView FAQ(HttpServletRequest request) {
		List<Faq> listFAQ = service.listFAQ();
		request.setAttribute("listFAQ", listFAQ);
		return new ModelAndView("/FAQ");
	}
	
	public boolean uploadFile(MyUploadForm myUploadForm, Model model, String location) {
	   	
   		MultipartFile file = myUploadForm.getFileDatas();
	   	if (file.isEmpty()) {
	   		model.addAttribute("message", "please choose file");
	   		return false;
	       }
	       try {
	
	           // Get the file and save it somewhere
	           byte[] bytes = file.getBytes();
	           Path path = Paths.get(location + file.getOriginalFilename());
	           Files.write(path, bytes);
	           model.addAttribute("message",
	                   "You successfully uploaded '" + file.getOriginalFilename() + "'");
	           
	           model.addAttribute("fileName",file.getOriginalFilename());
	       } catch (IOException e) {
	           e.printStackTrace();
	           return false;
	       }
			
	   		return true;
	   }
}
