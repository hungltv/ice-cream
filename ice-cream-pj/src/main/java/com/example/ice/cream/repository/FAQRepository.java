package com.example.ice.cream.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.ice.cream.entity.Faq;

public interface FAQRepository extends CrudRepository<Faq, Long> {

	
}
