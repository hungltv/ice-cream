package com.example.ice.cream.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class OnlineOrder {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "order_id", updatable = false, nullable = false)
	private Integer order_id;

	@NotNull
	@Size(min = 1, max = 50, message="name must be between 1 and 50 characters")
	private String name;
	@NotNull
	@Email
	@Size(min = 1, max = 50, message="email must be between 1 and 50 characters")
	private String email;
	@NotNull
	@Size(min = 1, max = 2048, message="contact must be between 1 and 2048 characters")
	private String contact;
	@NotNull
	@Size(min = 1, max = 2048, message="address must be between 1 and 2048 characters")
	private String address;
	
	private Double book_cost;
	
	@NotNull(message="paying_option must not be null")
	private String paying_option;
	
	@NotNull(message="order_date must not be null")
	private String order_date;
	
	private Integer status;

	public OnlineOrder() {
	}

	public OnlineOrder(Integer order_id, String name, String email, String contact, String address, Double book_cost,
					   String paying_option, String order_date, Integer status) {
		super();
		this.order_id = order_id;
		this.name = name;
		this.email = email;
		this.contact = contact;
		this.address = address;
		this.book_cost = book_cost;
		this.paying_option = paying_option;
		this.order_date = order_date;
		this.status = status;
	}

	public Integer getOrder_id() {
		return order_id;
	}

	public void setOrder_id(Integer order_id) {
		this.order_id = order_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Double getBook_cost() {
		return book_cost;
	}

	public void setBook_cost(Double book_cost) {
		this.book_cost = book_cost;
	}

	public String getPaying_option() {
		return paying_option;
	}

	public void setPaying_option(String paying_option) {
		this.paying_option = paying_option;
	}

	public String getOrder_date() {
		return order_date;
	}

	public void setOrder_date(String order_date) {
		this.order_date = order_date;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
