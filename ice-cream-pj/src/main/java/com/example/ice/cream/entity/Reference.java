package com.example.ice.cream.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Reference {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "reference_id", updatable = false, nullable = false)
	
	private Long reference_id;
	private Integer monthly_fee;
	private Integer yearly_fee;
	private Double book_cost;
	private String description;
	
	public Reference() {}
	
	public Reference(Long reference_id, Integer monthly_fee, Integer yearly_fee, Double book_cost, String description) {
		super();
		this.reference_id = reference_id;
		this.monthly_fee = monthly_fee;
		this.yearly_fee = yearly_fee;
		this.book_cost = book_cost;
		this.description = description;
	}

	public Long getReference_id() {
		return reference_id;
	}

	public void setReference_id(Long reference_id) {
		this.reference_id = reference_id;
	}

	public Integer getMonthly_fee() {
		return monthly_fee;
	}

	public void setMonthly_fee(Integer monthly_fee) {
		this.monthly_fee = monthly_fee;
	}

	public Integer getYearly_fee() {
		return yearly_fee;
	}

	public void setYearly_fee(Integer yearly_fee) {
		this.yearly_fee = yearly_fee;
	}

	public Double getBook_cost() {
		return book_cost;
	}

	public void setBook_cost(Double book_cost) {
		this.book_cost = book_cost;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
	

}
