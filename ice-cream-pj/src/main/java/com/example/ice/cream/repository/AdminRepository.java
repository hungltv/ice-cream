package com.example.ice.cream.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.example.ice.cream.entity.Admin;

public interface AdminRepository extends CrudRepository<Admin, Long> {
	
    @Query(value = "SELECT * FROM Admin ad where ad.username = :username and ad.password = :password", nativeQuery = true) 
	Admin getUsername(@Param("username") String username,@Param("password")  String password);
    
    
    @Modifying
	@Transactional
	@Query(value = "UPDATE Admin ad SET"
			+ " ad.password= :newPassword"
			+ " WHERE ad.username = :username and ad.password = :password", nativeQuery = true)
	void changePasswordAdmin(@Param("username") String username, @Param("password") String password, @Param("newPassword") String newPassword);
}
