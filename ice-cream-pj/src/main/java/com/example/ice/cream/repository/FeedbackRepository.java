package com.example.ice.cream.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.ice.cream.entity.Feedback;

public interface FeedbackRepository extends CrudRepository<Feedback, Long> {

	
}
