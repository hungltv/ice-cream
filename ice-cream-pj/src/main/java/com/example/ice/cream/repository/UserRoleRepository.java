package com.example.ice.cream.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.ice.cream.entity.UserRole;

public interface UserRoleRepository extends CrudRepository<UserRole, Long> {


}
