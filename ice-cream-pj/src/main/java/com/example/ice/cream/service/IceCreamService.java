package com.example.ice.cream.service;

import java.util.List;
import java.util.Optional;

import com.example.ice.cream.entity.Admin;
import com.example.ice.cream.entity.Customer;
import com.example.ice.cream.entity.Faq;
import com.example.ice.cream.entity.Feedback;
import com.example.ice.cream.entity.OnlineOrder;
import com.example.ice.cream.entity.Recipe;
import com.example.ice.cream.entity.Reference;
import com.example.ice.cream.entity.UserRole;
import com.example.ice.cream.entity.UserRecipe;

public interface IceCreamService {
	// get top 10 user recipes
	List<UserRecipe> listUserRecipe();
	// get top 10 most view recipes 
	List<Recipe> listRecipe(int limit);
	
	List<Recipe> listRecipeNew(int limit);

	// view detail
	Optional<Recipe> findById(Long id);
	
	List<Admin> checkAuthentication(String username, String password);
	// login user - check authentication
	List<Customer> listCustomer();
	
	Optional<Customer> findCustomerById(Integer customer_id);
	Customer findCustomerByUsername(String username);

	// get book infomation
	public Optional<Reference> referenceFirst(Integer reference_id);	
	// save order book
	OnlineOrder saveOrderBook(OnlineOrder online_order);
	
	// register user
	Customer saveCustomer(Customer customer);
	
	// save Role User
	public UserRole saveRoleUser(UserRole userRole);
	// update profile
	void updateCustomer(Customer customer);

	// save your post recipes
	UserRecipe savUserRecipe(UserRecipe user_recipse);
	
	void changePassword(Customer customer);

	List<Faq> listFAQ();

	Feedback savFeedback(Feedback feedback);
	
	Admin getUsername(String username, String password);
	
	void changePasswordAdmin(String username, String password, String newPassword);
	
	List<Customer> searchCustomer(String username);
	
	void updateCustomerDetail(Customer customer);

	Recipe saveNewRecipe(Recipe recipe);
	
	int updateRecipe(Recipe recipe);
	
	Optional<UserRecipe> getUserRecipeById(Long id);
	
	void updatePrizeStatus(UserRecipe user_recipe);

	int editUserRecipe(UserRecipe user_recipe);
	
	void deletedUserRecipe(Long user_recipe_id);
	
	List<OnlineOrder> lstOnlineOrderBook();
	
	Optional<OnlineOrder> getOnlineOrderById(Integer id);
	
	void updateStatusOrder(OnlineOrder online_order);
	
	List<Feedback> lstFeedback();
	
	void deletedFeedbackById(Long user_recipe_id);

	void deletedFAQById(Long faq_id);
	
	Faq saveFAQ(Faq faq);
	
	List<UserRecipe> fetchDataInnerJoin();
	
	List<UserRecipe> fetchUserRecipDesc();
	
	Optional<UserRecipe> fetchUserRecipeInnerJoinById(Long user_recipe_id);

	Optional<UserRole> findUserRole(Long id);

}
