package com.example.ice.cream.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.ice.cream.entity.Reference;

public interface ReferenceRepository extends CrudRepository<Reference, Long> {

	@Query(value = "select * from Reference re where re.reference_id = :reference_id", nativeQuery = true)
	Optional<Reference> referenceFirst(@Param("reference_id") int reference_id);
}
