package com.example.ice.cream.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Faq {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "faq_id", updatable = false, nullable = false)
	private Long faq_id;
	
	@NotNull
	@Size(min = 1, message="question must be more than 1 characters")
	private String question;
	
	@NotNull
	@Size(min = 1, message="answer must be more than 1 characters")
	private String answer;

	
	public Faq() {}

	public Faq(Long faq_id, String question, String answer) {
		super();
		this.faq_id = faq_id;
		this.question = question;
		this.answer = answer;
	}

	public Long getFaq_id() {
		return faq_id;
	}
	
	public void setFaq_id(Long faq_id) {
		this.faq_id = faq_id;
	}


	public String getQuestion() {
		return question;
	}


	public void setQuestion(String question) {
		this.question = question;
	}


	public String getAnswer() {
		return answer;
	}


	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	
	
}
