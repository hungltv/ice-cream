package com.example.ice.cream.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.example.ice.cream.entity.UserRecipe;

public interface UserRecipesRepository extends CrudRepository<UserRecipe, Long> {

	@Query(value = "select * from User_recipe re ORDER BY re.user_recipe_id DESC limit 10", nativeQuery = true)
	List<UserRecipe> findAll();
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE User_recipe ur SET"
			+ " ur.prize_status= :prize_status"
			+ " WHERE ur.user_recipe_id = :user_recipe_Id", nativeQuery = true)
	void updatePrizeStatus(@Param("prize_status") Integer prize_status, @Param("user_recipe_Id") Long user_recipe_Id);
	
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE User_recipe ur SET"
			+ " ur.name = :name,"
			+ " ur.desciption = :desciption,"
			+ " ur.details = :details,"
			+ " ur.image = :image,"
			+ " ur.enable_status = :enable_status "
			+ " WHERE ur.user_recipe_id = :user_recipe_id", nativeQuery = true)
	public int editUserRecipe(
			@Param("user_recipe_id") Long user_recipe_id,
			@Param("name") String name,
			@Param("desciption") String desciption,
			@Param("details") String details,
			@Param("image") String image,
			@Param("enable_status") int enable_status);
	
	@Query(value = "select * from User_recipe re inner join Customer cus ON cus.customer_id = re.customer_id ", nativeQuery = true)
	List<UserRecipe> fetchDataInnerJoin();
	
	
	@Query(value = "select * from User_recipe re inner join Customer cus ON cus.customer_id = re.customer_id order by re.user_recipe_id DESC limit 10", nativeQuery = true)
	List<UserRecipe> fetchUserRecipDesc();
	
	@Query(value = "select * from User_recipe re inner join Customer cus ON cus.customer_id = re.customer_id where re.user_recipe_id = :user_recipe_id", nativeQuery = true)
	Optional<UserRecipe> fetchUserRecipeInnerJoinById(@Param("user_recipe_id") Long user_recipe_id);
}
