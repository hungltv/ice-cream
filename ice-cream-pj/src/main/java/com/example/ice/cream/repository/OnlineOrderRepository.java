package com.example.ice.cream.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.example.ice.cream.entity.OnlineOrder;

public interface OnlineOrderRepository extends CrudRepository<OnlineOrder, Integer> {
	
	@Modifying // use for delete, update, insert
	@Transactional
	@Query(value = "UPDATE Online_order oo SET"
			+ " oo.status= :status"
			+ " WHERE oo.order_id = :order_id", nativeQuery = true) // nativeQuery = true de chuyen ve dang mySQL
	void updateStatusOrder(@Param("status") Integer status, @Param("order_id") Integer order_id);
	
}
