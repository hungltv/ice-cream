package com.example.ice.cream.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.example.ice.cream.entity.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
	@Modifying
	@Transactional
	@Query(value = "UPDATE Customer c SET"
			+ " c.full_name= :fullname,"
			+ " c.address = :address,"
			+ " c.phone_number = :phonenumber,"
			+ " c.address = :address,"
			+ " c.email = :email,"
			+ " c.gender = :gender,"
			+ " c.birthday = :birthday,"
			+ " c.avatar = :avatar,"
			+ " c.enable_status = :status,"
			+ " c.expired_date = :expirationDate "
			+ " WHERE c.customer_id = :customer_id", nativeQuery = true)
	void updateCustomer(
			@Param("customer_id") int customer_id,
			@Param("fullname") String fullname,
			@Param("address") String address,
			@Param("phonenumber") String phonenumber,
			@Param("email") String email,
			@Param("gender") String gender,
			@Param("birthday") String birthday,
			@Param("avatar") String avatar,
			@Param("status") int status,
			@Param("expirationDate") String expirationDate);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE Customer c SET"
			+ " c.password= :password"
			+ " WHERE c.customer_id = :customer_id", nativeQuery = true)
	void changePassword(@Param("password") String password, @Param("customer_id") int customer_id);
	
	@Query(value = "select * from Customer c where c.username LIKE %:username%", nativeQuery = true)
	List<Customer> searchUsername(@Param("username") String username);
	
	@Query(value = "select * from Customer c where c.username = :username", nativeQuery = true)
	Customer getByUserName(@Param("username") String username);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE Customer c SET"
			+ " c.gender = :gender,"
			+ " c.enable_status = :status,"
			+ " c.expired_date = :expirationDate "
			+ " WHERE c.customer_id = :customer_id", nativeQuery = true)
	void updateCustomerDetail(
			@Param("customer_id") int customer_id,
			@Param("gender") String gender,
			@Param("status") int status,
			@Param("expirationDate") String expirationDate);
}
