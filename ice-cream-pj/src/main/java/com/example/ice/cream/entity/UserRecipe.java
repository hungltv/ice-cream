package com.example.ice.cream.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class UserRecipe {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_recipe_id", updatable = false, nullable = false)
	private Long user_recipe_id;
	
	@NotNull
	@Size(min = 1, max = 100, message="name must be between 1 and 100 characters")
	private String name;
	
	
	private String image;
	
	@NotNull(message="desciption must not be null")
	@Size(min = 1, message="desciption must be more than 1 characters")
	private String desciption;
	
	@NotNull(message="desciption must not be null")
	@Size(min = 1, message="details must be more than 1 characters")
	private String details;
	
    @JoinColumn(name = "customer_id", nullable = false)
	private Integer customer_id;
	private Integer prize_status;
	private Integer enable_status;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer_id",  nullable = true, insertable = false, updatable = false)
	@Fetch(FetchMode.JOIN)
	private Customer customer;
	
	public UserRecipe() {}
	
	public UserRecipe(Long user_recipe_id, String name, String image, String desciption, String details,
					  Integer customer_id, Integer prize_status, Integer enable_status) {
		super();
		this.user_recipe_id = user_recipe_id;
		this.name = name;
		this.image = image;
		this.desciption = desciption;
		this.details = details;
		this.customer_id = customer_id;
		this.prize_status = prize_status;
		this.enable_status = enable_status;
	}

	public Long getUser_recipe_id() {
		return user_recipe_id;
	}

	public void setUser_recipe_id(Long user_recipe_id) {
		this.user_recipe_id = user_recipe_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDesciption() {
		return desciption;
	}

	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Integer getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Integer customer_id) {
		this.customer_id = customer_id;
	}

	public Integer getPrize_status() {
		return prize_status;
	}

	public void setPrize_status(Integer prize_status) {
		this.prize_status = prize_status;
	}

	public Integer getEnable_status() {
		return enable_status;
	}

	public void setEnable_status(Integer enable_status) {
		this.enable_status = enable_status;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
	
}
