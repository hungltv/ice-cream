package com.example.ice.cream.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.ice.cream.dao.AppRoleDAO;
import com.example.ice.cream.entity.Customer;
import com.example.ice.cream.repository.CustomerRepository;

@Service
public class MyUserDetailsService implements UserDetailsService{

    private Logger logger = LoggerFactory.getLogger(MyUserDetailsService.class);

	@Autowired
    private CustomerRepository customerRepository;
	
	@Autowired
	private AppRoleDAO appRoleDAO;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Customer user = customerRepository.getByUserName(username);
    
        if (user == null) {
            logger.info("Not found user: {}", username);
            throw new UsernameNotFoundException("User " + username + " was not found in the database");
        }

        logger.info("Found User: {}", user.getFull_name());
        // [ROLE_USER, ROLE_ADMIN,..]
        List<String> roleNames = this.appRoleDAO.getRoleNames(user.getCustomer_id());

        logger.info("RolesName: {}", roleNames);
        List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
        if (roleNames != null) {
            for (String role : roleNames) {
                // ROLE_USER, ROLE_ADMIN,..
                GrantedAuthority authority = new SimpleGrantedAuthority(role);
                grantList.add(authority);
            }
        }
 
        UserDetails userDetails =  new User(user.getUsername(),
                user.getPassword(), grantList);

        return userDetails;
      
    }
}
