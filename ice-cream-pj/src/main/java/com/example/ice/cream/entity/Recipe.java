package com.example.ice.cream.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.ColumnDefault;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Recipe {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "recipe_id", updatable = false, nullable = false)
	private Long recipe_id;
	
	@NotNull(message="name must not be null")
	@Size(min = 1, max = 100, message="name must be between 1 and 100 characters")
	private String name;
	
	private String image;
	
	@NotNull(message="desciption must not be null")
	@Size(min = 1, message="desciption must be more than 1 characters")
	private String desciption;
	
	@NotNull(message="details must not be null")
	@Size(min = 1, message="details must be more than 1 characters")
	private String details;
	
	@NotNull(message="author must not be null")
	@Size(min = 1, max = 50, message="author must be between 1 and 50 characters")
	private String author;
	
	private Integer view_number;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@NotNull(message="upload_date must not be null")
	private String upload_date;
		
	@ColumnDefault("0")
	private Integer enable_status;
	
	public Recipe() {}
	public Recipe(Long recipe_id, String name, String image, String desciption, String details, String author,
			Integer view_number, String upload_date, Integer enable_status) {
		super();
		this.recipe_id = recipe_id;
		this.name = name;
		this.image = image;
		this.desciption = desciption;
		this.details = details;
		this.author = author;
		this.view_number = view_number;
		this.upload_date = upload_date;
		this.enable_status = enable_status;
	}
	public Long getRecipe_id() {
		return recipe_id;
	}
	public void setRecipe_id(Long recipe_id) {
		this.recipe_id = recipe_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getDesciption() {
		return desciption;
	}
	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Integer getView_number() {
		return view_number;
	}
	public void setView_number(Integer view_number) {
		this.view_number = view_number;
	}
	public String getUpload_date() {
		return upload_date;
	}
	public void setUpload_date(String upload_date) {
		this.upload_date = upload_date;
	}
	public Integer getEnable_status() {
		return enable_status;
	}
	public void setEnable_status(Integer enable_status) {
		this.enable_status = enable_status;
	}

	
	
}
