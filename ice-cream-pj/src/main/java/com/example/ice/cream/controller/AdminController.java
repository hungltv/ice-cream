package com.example.ice.cream.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.dao.DataAccessException;
import org.springframework.data.repository.query.Param;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.example.ice.cream.entity.Admin;
import com.example.ice.cream.entity.Customer;
import com.example.ice.cream.entity.Faq;
import com.example.ice.cream.entity.Feedback;
import com.example.ice.cream.entity.MyCustomers;
import com.example.ice.cream.entity.MyUploadForm;
import com.example.ice.cream.entity.OnlineOrder;
import com.example.ice.cream.entity.Recipe;
import com.example.ice.cream.entity.UserRecipe;
import com.example.ice.cream.service.IceCreamService;

@RestController
public class AdminController {

	@Autowired
	private IceCreamService service;
	
	@GetMapping("/admin-login")
	public ModelAndView adminLogin(HttpServletRequest request) {	
		return new ModelAndView("admin/admin-login");
	}
	private static final String UPLOADED_FOLDER = "./src/main/resources/static/images/uploaded/";
	private static final String USER_RECIPE_FOLDER = "./src/main/resources/static/images/userrecipe/";
	private static final String IMAGES_FOLDER = "./src/main/resources/static/images/";

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

	@PostMapping("/admin-login")
	public ModelAndView login(
			@Param("username") String username,
			@Param("password") String password,
			HttpServletRequest request) {
		Admin admin = service.getUsername(username, password);
		
		String usernameReturn = "";
		String passwordReturn = "";
		if(admin != null) {
			 usernameReturn = admin.getUsername() == null ? "" : admin.getUsername();
			 passwordReturn = admin.getPassword() == null ? "" : admin.getPassword();
		}

		ModelAndView mav = null;
		
		if(usernameReturn.equals(username) && passwordReturn.equals(password)) {
			mav = new ModelAndView("admin/changePassword");
			request.setAttribute("username", username);
			request.setAttribute("password", password);
			request.setAttribute("login", "true");
		} else {
			mav = new ModelAndView("admin/admin-login");
			request.setAttribute("message", "wrong username or password !");
		}
		
		return mav;
	}
	
	@GetMapping("/messageChangePassword")
	public ModelAndView messageChangePassword() {
		return new ModelAndView("admin/messageChangePassword");
	}

	
	@GetMapping("/view-changePassword")
	public ModelAndView viewChangePassword() {
		return new ModelAndView("admin/changePassword");
	}
	
	@PostMapping("/admin-changePassword")
	public ModelAndView changePassword(
			@Param("username") String username,
			@Param("oldPassword") String oldPassword,
			@Param("newPassword") String newPassword,
			@Param("rePassword") String rePassword,
			HttpServletRequest request
			) {
		ModelAndView mav = null;
		Admin admin = service.getUsername(username, oldPassword);
		
		if(admin == null) {
			request.setAttribute("message", "Old password invalid !");
			request.setAttribute("username", username);
			mav = new ModelAndView("admin/changePassword");
			return mav;
		}
		
		if(!newPassword.equals(rePassword)) {
			request.setAttribute("message", "Password doesn't match !");
			request.setAttribute("username", username);
			mav = new ModelAndView("admin/changePassword");
			return mav;
		}
		
		service.changePasswordAdmin(username, oldPassword, newPassword );
		return new ModelAndView("redirect:/view-adminCustomerMana");
	}
	
	
	@GetMapping("/view-adminCustomerMana")
	public ModelAndView viewaAminCustomerMana(HttpServletRequest request, Model model) {
		
		List<Customer> listCus = service.listCustomer();
		request.setAttribute("listCus", listCus);
		request.setAttribute("total", listCus.size());
		request.setAttribute("sortby", "customer_id");

		MyCustomers myCustomers = new MyCustomers();
		myCustomers.setCustomers(listCus);
		model.addAttribute("myCustomers", myCustomers);
		
		return new ModelAndView("admin/adminCustomerMana");
	}
	
	@PostMapping("/searchUsername")
	public ModelAndView searchKeyword(@Param("keyword") String keyword,HttpServletRequest request, Model model) {
		
		List<Customer> retlistCustomer = service.searchCustomer(keyword);
		request.setAttribute("listCus", retlistCustomer);
		request.setAttribute("total", retlistCustomer.size());
		request.setAttribute("keyword", keyword);
		
		MyCustomers myCustomers = new MyCustomers();
		myCustomers.setCustomers(retlistCustomer);
		model.addAttribute("myCustomers", myCustomers);
		
		return new ModelAndView("admin/adminCustomerMana");
	}
	
	@PostMapping("/updateCustomer")
	public ModelAndView updateAllCustomer(MyCustomers myCustomers, HttpServletRequest request) {
		
		return new ModelAndView("admin/adminCustomerMana");
	}
	
	
	@PostMapping("/sortby")
	public ModelAndView sortby(@Param("keyword") String keyword ,@Param("sortby") String sortby, HttpServletRequest request, Model model) {
		
		List<Customer> listCus = service.searchCustomer(keyword);
		if(sortby.equals("customer_id")) {
			Comparator<Customer> compareById = (Customer o1, Customer o2) -> o1.getCustomer_id().compareTo( o2.getCustomer_id() );
			Collections.sort(listCus, compareById);
			Collections.sort(listCus, compareById.reversed());;
		} else if(sortby.equals("username")) {
			Comparator<Customer> compareByUserName = (Customer o1, Customer o2) -> o1.getUsername().compareTo( o2.getUsername() );
			Collections.sort(listCus, compareByUserName);
		
		} else if(sortby.equals("expired_date")) {
			Comparator<Customer> compareExpiredDate = (Customer o1, Customer o2) -> o1.getExpired_date().compareTo( o2.getExpired_date() );
			Collections.sort(listCus, compareExpiredDate);
		}
	
		
		request.setAttribute("listCus", listCus);
		request.setAttribute("total", listCus.size());
		request.setAttribute("keyword", keyword);
		request.setAttribute("sortby", sortby);
		
		MyCustomers myCustomers = new MyCustomers();
		myCustomers.setCustomers(listCus);
		model.addAttribute("myCustomers", myCustomers);
		
		return new ModelAndView("admin/adminCustomerMana");
	}
	
	@GetMapping("/view-adminCustomerDetails")
	public ModelAndView viewaAminCustomerDetails(HttpServletRequest request, Model model) {
		
		String customer_id = request.getParameter("customer_id");
		Optional<Customer> retData = service.findCustomerById(Integer.parseInt(customer_id));
		Customer customer = retData.get();
		request.setAttribute("username", customer.getUsername());
		request.setAttribute("full_name", customer.getFull_name());
		request.setAttribute("address", customer.getAddress());
		request.setAttribute("phone_number", customer.getPhone_number());
		request.setAttribute("email", customer.getEmail());
		request.setAttribute("gender", customer.getGender());
		request.setAttribute("birthday", customer.getBirthday());
		request.setAttribute("expired_date", customer.getExpired_date());

		model.addAttribute("customer", customer);
		return new ModelAndView("admin/adminCustomerDetails");
	}
	
	@PostMapping("/updateCustomerDetail")
	public ModelAndView updateCustomerDetail(HttpServletRequest request, Customer customer, @Param("plus_expired_date") String plus_expired_date) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String expired_date = customer.getExpired_date();
		LocalDate expired_date_to_localDate = LocalDate.parse(expired_date, formatter).plusDays(Long.parseLong(plus_expired_date));
		
		String dateFinal = expired_date_to_localDate.format(DateTimeFormatter.ISO_DATE);
		customer.setExpired_date(dateFinal);
		service.updateCustomerDetail(customer);

		return new ModelAndView("redirect:/view-adminCustomerMana");
	}
	
	
	@GetMapping("/view-adminRecipesMana")
	public ModelAndView viewaAdminRecipes(HttpServletRequest request) {		
		List<Recipe> lstRecipe = service.listRecipeNew(10);
		request.setAttribute("listRecipe", lstRecipe);
		return new ModelAndView("admin/adminRecipesMana");
	}
	
	@GetMapping("/edit-recipe")
	public ModelAndView viewReipe(HttpServletRequest request, Model model) {
		String recipe_Id = request.getParameter("recipe_id");
		Optional<Recipe> retRecipe =  service.findById(Long.parseLong(recipe_Id));
		Recipe recipe = retRecipe.get();
		
		model.addAttribute("myUploadForm", new MyUploadForm());
		model.addAttribute("recipe", recipe);
		return new ModelAndView("component/edit-recipe");
	}
	
	@PostMapping("/edit-recipe")
	public ModelAndView editReipe(HttpServletRequest request, MyUploadForm myUploadForm, 
			@Valid Recipe recipe, BindingResult bindingResult,  Model model) {
		
		model.addAttribute("myUploadForm", new MyUploadForm());
		model.addAttribute("recipe", recipe);
		
		boolean ret = uploadFile(myUploadForm, model, IMAGES_FOLDER);
		if(ret) {
			recipe.setImage("images/" + model.getAttribute("fileName"));

//			model.addAttribute("checkfile", model.getAttribute("message"));
//			return new ModelAndView("/component/edit-recipe");
		}
		if (bindingResult.hasErrors()) {
			return new ModelAndView("/component/edit-recipe");
		}
		try {
			if(StringUtils.isEmpty(recipe.getEnable_status())) {
				recipe.setEnable_status(0);
			}
			service.updateRecipe(recipe);
        } catch (DataAccessException ex) {
			model.addAttribute("result", ex.getCause().getMessage());
        	LOGGER.error(ex.getCause().getMessage());
			return new ModelAndView("/component/edit-recipe");
        }
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/view-adminRecipesMana");
	}
	
	
	@GetMapping("/preView")
	public ModelAndView preView(HttpServletRequest request, Model model) {
		String recipe_Id = request.getParameter("recipe_id");
		Optional<Recipe> retRecipe =  service.findById(Long.parseLong(recipe_Id));
		Recipe recipe = retRecipe.get();
		
		model.addAttribute("recipe", recipe);
		return new ModelAndView("component/preView");
	}
	
	@GetMapping("/view-adminAddNewRecipe")
	public ModelAndView viewAdminAddNewRecipe(HttpServletRequest request, Model model) {
		model.addAttribute("myUploadForm", new MyUploadForm());
		Recipe recipe = new Recipe();
		model.addAttribute("recipe", recipe);
		return new ModelAndView("admin/adminAddNewRecipe");
	}
	
	@PostMapping("/preViewNewRecipe")
	public ModelAndView preViewNewRecipe(HttpServletRequest request, MyUploadForm myUploadForm,
			@Valid Recipe recipe, BindingResult bindingResult, Model model) {

		
		model.addAttribute("recipe", recipe);
		model.addAttribute("myUploadForm", myUploadForm);
		boolean ret = uploadFile(myUploadForm, model, UPLOADED_FOLDER);
		if(ret) {
			recipe.setImage("images/uploaded/" + model.getAttribute("fileName"));
		} else {
			recipe.setImage("images/uploaded/noavatar.gif");
		}
		
		if (bindingResult.hasErrors()) {
			return new ModelAndView("admin/adminAddNewRecipe");
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		return new ModelAndView("component/preViewAddNewRecipe");
	}
	
	
	@PostMapping("/saveNewRecipe")
	public ModelAndView saveNewRecipe(HttpServletRequest request, 
			@Valid Recipe recipe, BindingResult bindingResult,  Model model) {
		
		model.addAttribute("myUploadForm", new MyUploadForm());
		model.addAttribute("recipe", recipe);
		
		
		if (bindingResult.hasErrors()) {
			return new ModelAndView("admin/adminAddNewRecipe");
		}
		
		try {
			String updateDate = LocalDate.now().format(DateTimeFormatter.ISO_DATE);
			recipe.setUpload_date(updateDate);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			recipe.setView_number(1);
			if(StringUtils.isEmpty(recipe.getEnable_status())){
				recipe.setEnable_status(0);
			}
			service.saveNewRecipe(recipe);
		} catch (DataAccessException ex) {
			model.addAttribute("result", ex.getCause().getMessage());
        	LOGGER.error(ex.getCause().getMessage());
			return new ModelAndView("admin/adminAddNewRecipe");
		}
		
		return new ModelAndView("redirect:/view-adminRecipesMana");
	}
	
	
	
	
	@GetMapping("/view-adminUsersRecipesMana")
	public ModelAndView viewAdminUsersRecipesMana(HttpServletRequest request, Model model) {
		
		List<UserRecipe> listUserRecip = service.listUserRecipe();
		model.addAttribute("listUserRecip", listUserRecip);
		return new ModelAndView("admin/adminUsersRecipesMana");
	}
	
	
	@GetMapping("/view-edit-UserRecipe")
	public ModelAndView viewUserRecipe(HttpServletRequest request, Model model) {
		
		String user_recipe_id = request.getParameter("user_recipe_id");
		Optional<UserRecipe> retData = service.fetchUserRecipeInnerJoinById(Long.parseLong(user_recipe_id));
		UserRecipe user_recipe = retData.get();
		
		
		
		model.addAttribute("myUploadForm", new MyUploadForm());
		model.addAttribute("user_recipe", user_recipe);
		return new ModelAndView("component/edit-user-recipe");
	}
	
	@PostMapping("/saveEditUserRecipe")
	public ModelAndView saveEditUserRecipe(MyUploadForm myUploadForm,
                                           @Valid UserRecipe user_recipe, BindingResult bindingResult, Model model) {
		
		// để khi có lỗi thì vẫn giữ nguyên giá trị đã nhập trước đó
		model.addAttribute("myUploadForm", new MyUploadForm());
		model.addAttribute("user_recipe", user_recipe);
		
		boolean ret = uploadFile(myUploadForm, model, USER_RECIPE_FOLDER);
		if(ret) {
			user_recipe.setImage("images/userrecipe/" + model.getAttribute("fileName"));
		}
		
		if (bindingResult.hasErrors()) {
			return new ModelAndView("/component/edit-user-recipe");
		}
		
		try {
			if(StringUtils.isEmpty(user_recipe.getEnable_status())) {
				user_recipe.setEnable_status(0);
			}
			service.editUserRecipe(user_recipe);
        } catch (DataAccessException ex) {
			model.addAttribute("result", ex.getCause().getMessage());
        	LOGGER.error(ex.getCause().getMessage());
			return new ModelAndView("/component/edit-user-recipe");
        }
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/view-adminUsersRecipesMana");
	}
	
	@PostMapping("/deleted-UserRecip")
	public ModelAndView deletedUserRecip(HttpServletRequest request, Model model, @Param("user_recipe_id") Long user_recipe_id) {
		
		try {
			service.deletedUserRecipe(user_recipe_id);
        } catch (DataAccessException ex) {
			model.addAttribute("resultDelete", ex.getCause().getMessage());
        	LOGGER.error(ex.getCause().getMessage());
			return new ModelAndView("/admin/adminUsersRecipesMana");
        }
		
		return new ModelAndView("redirect:/view-adminUsersRecipesMana");
	}
	
	
	
	@GetMapping("/view-adminOnlineOrderMana")
	public ModelAndView viewAdminOnlineOrderMana(HttpServletRequest request, Model model) {
		request.getSession().setAttribute("listOnlineOrder", null);

		return new ModelAndView("redirect:/onlineOrder/page/1");
	}
	
	/**phan trang*/
	@GetMapping("/onlineOrder/page/{pageNumber}")
	public ModelAndView showEmployeePage(HttpServletRequest request, 
			@PathVariable int pageNumber, Model model) {
		PagedListHolder<?> pages = (PagedListHolder<?>) request.getSession().getAttribute("listOnlineOrder");
		int pagesize = 10;
		
		List<OnlineOrder> list = service.lstOnlineOrderBook();
		
		if (pages == null) {
			pages = new PagedListHolder<>(list);
			pages.setPageSize(pagesize);
		} else {
			final int goToPage = pageNumber - 1;
			if (goToPage <= pages.getPageCount() && goToPage >= 0) {
				pages.setPage(goToPage);
			}
		}
		request.getSession().setAttribute("listOnlineOrder", pages);
		int current = pages.getPage() + 1;
		int begin = Math.max(1, current - list.size());
		int end = Math.min(begin + 5, pages.getPageCount());
		int totalPageCount = pages.getPageCount();
		String baseUrl = "/onlineOrder/page/";

		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);
		model.addAttribute("totalPageCount", totalPageCount);
		model.addAttribute("baseUrl", baseUrl);
		model.addAttribute("onlineOrders", pages);

		return new ModelAndView("admin/adminOnlineOrderMana");
	}
	
	/**end phan trang*/
	
	
	@GetMapping("/view-adminOrderDetails")
	public ModelAndView viewAdminOrderDetails(HttpServletRequest request, Model model) {
		try {
			String order_id = request.getParameter("order_id");
			Optional<OnlineOrder> retData =  service.getOnlineOrderById(Integer.parseInt(order_id));
			OnlineOrder online_order = retData.get();
			
			model.addAttribute("online_order", online_order);
		} catch (Exception e) {
		}
		
		return new ModelAndView("admin/adminOrderDetails");
	}
	
	@PostMapping("/update_status_order")
	public ModelAndView update_status_order(HttpServletRequest request, Model model, OnlineOrder online_order) {
		
		try {
			service.updateStatusOrder(online_order);
		} catch (DataAccessException ex) {
			model.addAttribute("result", ex.getCause().getMessage());
        	LOGGER.error(ex.getCause().getMessage());
		}
		
		return new ModelAndView("redirect:/view-adminOnlineOrderMana");
	}
	
	@GetMapping("/view-adminFeedbackFAQMana")
	public ModelAndView viewAdminFeedbackFAQMana(HttpServletRequest request, Model model) {
		
		List<Feedback> listFeedback = service.lstFeedback();
		model.addAttribute("listFeedback", listFeedback);
		return new ModelAndView("admin/adminFeedbackFAQMana");
	}
	
	@PostMapping("/deleteFeedback")
	public ModelAndView deleteFeedback(HttpServletRequest request, Model model, @Param("feedback_id") Long feedback_id) {
		service.deletedFeedbackById(feedback_id);
		return new ModelAndView("redirect:/view-adminFeedbackFAQMana");
	}
	
	
	@GetMapping("/frequentlyAskedQuestion")
	public ModelAndView frequentlyAskedQuestion(HttpServletRequest request, Model model) {
		List<Faq> listFAQ = service.listFAQ();
		model.addAttribute("listFAQ", listFAQ);
		return new ModelAndView("component/frequentlyAskedQuestion");
	}
	
	@PostMapping("/deleteFAQ")
	public ModelAndView deleteFAQ(@Param("feedback_id") Long faq_id) {
		service.deletedFAQById(faq_id);
		return new ModelAndView("redirect:/frequentlyAskedQuestion");
	}
	
	
	@GetMapping("/addNewFAQ")
	public ModelAndView addNewFAQ(HttpServletRequest request, Model model) {
		
		Faq faq = new Faq();
		model.addAttribute("faq", faq);
		
		return new ModelAndView("component/addNewFAQ");
	}
	
	@PostMapping("/saveFAQ")
	public ModelAndView saveFAQ(HttpServletRequest request, Model model, @Valid Faq faq, BindingResult bindingResult) {
		
		model.addAttribute("FAQ", faq);
		
		if(bindingResult.hasErrors()) {
			return new ModelAndView("component/addNewFAQ");
		}
		try {
			service.saveFAQ(faq);
		} catch (DataAccessException ex) {
			model.addAttribute("result", ex.getCause().getMessage());
        	LOGGER.error(ex.getCause().getMessage());
		}
		
		return new ModelAndView("redirect:/frequentlyAskedQuestion");
	}
	
	@GetMapping("/view-adminPrizeMana")
	public ModelAndView adminPrizeMana(HttpServletRequest request, Model model) {
		
		List<UserRecipe> listUserRecip = service.fetchDataInnerJoin();
		model.addAttribute("listUserRecip", listUserRecip);
		return new ModelAndView("admin/adminPrizeMana");
	}
	
	@GetMapping("/view-adminUserRecipePrizedDetail")
	public ModelAndView adminUserRecipePrizedDetail(HttpServletRequest request, Model model) {
		
		try {
			String user_recipe_id = request.getParameter("user_recipe_id");
			Optional<UserRecipe> retData = service.getUserRecipeById(Long.parseLong(user_recipe_id));
			UserRecipe user_recipe = retData.get();
			model.addAttribute(user_recipe);
		} catch (Exception e) {
		}
		
		return new ModelAndView("admin/adminUserRecipePrizedDetail");
	}

	@GetMapping("/authorDetail")
	public ModelAndView authorDetail(HttpServletRequest request, Model model) {
		try {
			String customer_id = request.getParameter("customer_id");
			Optional<Customer> retData = service.findCustomerById(Integer.parseInt(customer_id));
			Customer customer = retData.get();
			model.addAttribute(customer);
		} catch (Exception e) {
		}
		
		return new ModelAndView("component/authorDetail");
	}
	
	@PostMapping("/update_prize_status")
	public ModelAndView update_prize_status(HttpServletRequest request, Model model, UserRecipe user_recipe) {
		
		try {
			service.updatePrizeStatus(user_recipe);
		} catch (DataAccessException ex) {
			model.addAttribute("result", ex.getCause().getMessage());
        	LOGGER.error(ex.getCause().getMessage());
		}
		
		return new ModelAndView("redirect:/view-adminUsersRecipesMana");
	}
	
	
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public boolean uploadFile(MyUploadForm myUploadForm, Model model, String location) {
   	
   		MultipartFile file = myUploadForm.getFileDatas();
	   	if (file.isEmpty()) {
	   		model.addAttribute("message", "please choose file");
	   		return false;
	       }
	       try {
	
	           // Get the file and save it somewhere
	           byte[] bytes = file.getBytes();
	           Path path = Paths.get(location + file.getOriginalFilename());
	           Files.write(path, bytes);
	           model.addAttribute("message",
	                   "You successfully uploaded '" + file.getOriginalFilename() + "'");
	           
	           model.addAttribute("fileName",file.getOriginalFilename());
	       } catch (IOException e) {
	           e.printStackTrace();
	           return false;
	       }
			
	   		return true;
	   }
	 
}
