package com.example.ice.cream.entity;

import java.util.List;


public class MyCustomers {

	private List<Customer> customers;
	
	
	
	public MyCustomers() {}
	
	public MyCustomers(List<Customer> customers) {
		super();
		this.customers = customers;
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}


}
