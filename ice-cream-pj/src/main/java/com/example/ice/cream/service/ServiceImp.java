package com.example.ice.cream.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.example.ice.cream.entity.Admin;
import com.example.ice.cream.entity.Customer;
import com.example.ice.cream.entity.Faq;
import com.example.ice.cream.entity.Feedback;
import com.example.ice.cream.entity.OnlineOrder;
import com.example.ice.cream.entity.Recipe;
import com.example.ice.cream.entity.Reference;
import com.example.ice.cream.entity.UserRole;
import com.example.ice.cream.entity.UserRecipe;
import com.example.ice.cream.repository.AdminRepository;
import com.example.ice.cream.repository.CustomerRepository;
import com.example.ice.cream.repository.FAQRepository;
import com.example.ice.cream.repository.FeedbackRepository;
import com.example.ice.cream.repository.OnlineOrderRepository;
import com.example.ice.cream.repository.RecipeRepository;
import com.example.ice.cream.repository.ReferenceRepository;
import com.example.ice.cream.repository.UserRecipesRepository;
import com.example.ice.cream.repository.UserRoleRepository;

@Service
@Repository
public class ServiceImp implements IceCreamService {
	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	UserRecipesRepository userRecipesRepository;
	
	@Autowired
	RecipeRepository recipeRepository;
	
	@Autowired
	AdminRepository adminRepository;
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	ReferenceRepository referenceRepository;
	
	@Autowired
	OnlineOrderRepository onlineOrderRepository;
	
	@Autowired
	FAQRepository faqRepository;
	
	@Autowired
	FeedbackRepository feedbackRepository;
	@Autowired
	UserRoleRepository userRoleRepository;
	
	
	@Override
	public List<UserRecipe> listUserRecipe() {
		return (List<UserRecipe>) userRecipesRepository.findAll();
	}

	@Override
	public List<Recipe> listRecipeNew(int limit) {
		return (List<Recipe>) recipeRepository.findAllDesc(limit);
	}
	
	@Override
	public List<Recipe> listRecipe(int limit) {
		return (List<Recipe>) recipeRepository.findAll(limit);
	}
	
	@Override
	public Optional<Recipe> findById(Long id) {
		return (Optional<Recipe>) recipeRepository.findById(id);
	}

	@Override
	public List<Admin> checkAuthentication(String username, String password) {
//		return (List<Admin>) adminRepository.getUsername(username, password);
		return null;
	}
	
	@Override
	public List<Customer> listCustomer() {
		return (List<Customer>) customerRepository.findAll();
	}

	@Override
	public Optional<Reference> referenceFirst(Integer reference_id) {
		return (Optional<Reference>) referenceRepository.referenceFirst(reference_id);
	}

	@Override
	public OnlineOrder saveOrderBook(OnlineOrder online_order) {
		return (OnlineOrder) onlineOrderRepository.save(online_order);
	}

	@Override
	public Customer saveCustomer(Customer customer) {
		return (Customer) customerRepository.save(customer);
	}
	@Override
	public UserRole saveRoleUser(UserRole userRole) {
		return (UserRole) userRoleRepository.save(userRole);
	};


	@Override
	public Optional<Customer> findCustomerById(Integer customer_id) {
		return (Optional<Customer>) customerRepository.findById(customer_id);
	}
	
	@Override
	public Customer findCustomerByUsername(String username) {
		
		return customerRepository.getByUserName(username);
	};
	
	@Override
	public void updateCustomer(Customer customer) {
		Integer id = customer.getCustomer_id();
		String fullname = customer.getFull_name();
		String address = customer.getAddress();
		String phonenumber = customer.getPhone_number();
		String email = customer.getEmail();
		String gender = customer.getGender();
		String birthday = customer.getBirthday();
		String avatar = customer.getAvatar();
		Integer status = customer.getEnable_status();
		String expirationDate = customer.getExpired_date();
		
		customerRepository.updateCustomer(id, fullname, address, 
				phonenumber, email, gender, birthday, avatar, status, expirationDate);
		
	}

	@Override
	public UserRecipe savUserRecipe(UserRecipe user_recipse) {
		return (UserRecipe) userRecipesRepository.save(user_recipse);
	}
	
	@Override
	public void changePassword(Customer customer) {
		String password = customer.getPassword();
		Integer customer_id = customer.getCustomer_id();
		customerRepository.changePassword(password, customer_id);
		
	}

	@Override
	public List<Faq> listFAQ() {
		return (List<Faq>) faqRepository.findAll();
	}
	
	
	@Override
	public Feedback savFeedback(Feedback feedback) {
		return (Feedback) feedbackRepository.save(feedback);
	}

	// admin
	@Override
	public Admin getUsername(String username, String password) {
		return (Admin) adminRepository.getUsername(username, password);
	}

	@Override
	public void changePasswordAdmin(String username, String password, String newPassword) {
		adminRepository.changePasswordAdmin(username, password, newPassword);
	}

	@Override
	public List<Customer> searchCustomer(String username) {
		return (List<Customer>) customerRepository.searchUsername(username);
	}

	@Override
	public void updateCustomerDetail(Customer customer) {
		String expirationDate = customer.getExpired_date();
		Integer customer_id = customer.getCustomer_id();
		String gender = customer.getGender();
		Integer status = customer.getEnable_status();

		customerRepository.updateCustomerDetail(customer_id, gender, status, expirationDate);
	}

	@Override
	public Recipe saveNewRecipe(Recipe recipe) {
		return recipeRepository.save(recipe);
	}

	@Override
	public int updateRecipe(Recipe recipe) {
		Long recipe_id = recipe.getRecipe_id();
		String name = recipe.getName();
		String desciption = recipe.getDesciption();
		String details = recipe.getDetails();
		String author = recipe.getAuthor();
		String image = recipe.getImage();
		Integer enable_status = recipe.getEnable_status();
		return recipeRepository.updateRecipe(recipe_id, name, desciption, details, author, image, enable_status);
	}

	public Optional<UserRecipe> getUserRecipeById(Long id){
		
		return (Optional<UserRecipe>) userRecipesRepository.findById(id);
	}

	@Override
	public void updatePrizeStatus(UserRecipe user_recipe) {
		Integer prize_status = user_recipe.getPrize_status();
		Long user_recipe_Id = user_recipe.getUser_recipe_id();
		userRecipesRepository.updatePrizeStatus(prize_status, user_recipe_Id);
	}

	@Override
	public int editUserRecipe(UserRecipe user_recipe) {
		
		Long user_recipe_id = user_recipe.getUser_recipe_id();
		String name = user_recipe.getName();
		String desciption = user_recipe.getDesciption();
		String details = user_recipe.getDetails();
		String image = user_recipe.getImage();
		Integer enable_status = user_recipe.getEnable_status();
		return userRecipesRepository.editUserRecipe(user_recipe_id, name, desciption, details, image, enable_status);
	}

	@Override
	public void deletedUserRecipe(Long user_recipe_id) {
		 userRecipesRepository.deleteById(user_recipe_id);
	}
	
	@Override
	public List<OnlineOrder> lstOnlineOrderBook(){
		return (List<OnlineOrder>) onlineOrderRepository.findAll();
	};
	
	@Override
	public Optional<OnlineOrder> getOnlineOrderById(Integer id){
		
		return (Optional<OnlineOrder>) onlineOrderRepository.findById(id);
	};	
	
	@Override
	public void updateStatusOrder(OnlineOrder online_order) {
		Integer status = online_order.getStatus();
		Integer order_id = online_order.getOrder_id();
		onlineOrderRepository.updateStatusOrder(status, order_id);
	};
	
	@Override
	public List<Feedback> lstFeedback(){
		return (List<Feedback>) feedbackRepository.findAll();
	};
	
	@Override
	public void deletedFeedbackById(Long user_recipe_id) {
		feedbackRepository.deleteById(user_recipe_id);
	};
	
	@Override
	public void deletedFAQById(Long faq_id) {
		faqRepository.deleteById(faq_id);
	};
	
	@Override
	public Faq saveFAQ(Faq faq) {
		return faqRepository.save(faq);
	};
	
	@Override
	public List<UserRecipe> fetchDataInnerJoin(){
		
		return userRecipesRepository.fetchDataInnerJoin();
	};
	
	@Override
	public List<UserRecipe> fetchUserRecipDesc(){
		
		return userRecipesRepository.fetchUserRecipDesc();
	};

	@Override
	public Optional<UserRecipe> fetchUserRecipeInnerJoinById(Long user_recipe_id){
		
		return userRecipesRepository.fetchUserRecipeInnerJoinById(user_recipe_id);
	}

	@Override
	public Optional<UserRole> findUserRole(Long id){
		return userRoleRepository.findById(id);
	}
}
